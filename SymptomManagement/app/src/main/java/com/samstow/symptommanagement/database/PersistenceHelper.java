package com.samstow.symptommanagement.database;

import android.content.Context;

import com.j256.ormlite.android.apptools.OpenHelperManager;
import com.j256.ormlite.dao.Dao;

import org.androidannotations.annotations.EBean;

import java.lang.reflect.Field;
import java.sql.SQLException;
import java.util.List;

/**
 * Created by samstow on 8/11/14.
 */
@EBean
public class PersistenceHelper<T, ID> {

    private SymptomManagementHelper symptomManagementHelper;

    private static String SERVER_ID = "serverId";

    public PersistenceHelper(Context context) {
        symptomManagementHelper = OpenHelperManager.getHelper(context, SymptomManagementHelper.class);
    }

    public Dao<T, ID> getDao(Class clazz) {
        try {
            return symptomManagementHelper.getDao(clazz);
        } catch (SQLException ex) {
            Class superClass = clazz.getSuperclass();
            if (superClass != null) {
                return getDao(superClass);
            } else {
                return null;
            }
        }
    }


    public T createOrRetrieve(T object) {
        return createOrRetrieve(SERVER_ID, object);
    }

    private Field getFieldFromClass(String fieldName, Class clazz) {
        Field field = null;
        try {
            field = clazz.getDeclaredField(fieldName);
        } catch (Exception ex) {
            Class superClass = clazz.getSuperclass();
            if (superClass != null) {
                return getFieldFromClass(fieldName, superClass);
            }
        }
        field.setAccessible(true);
        return field;
    }

    public T createOrRetrieve(String fieldName, T updated) throws RuntimeException {
        T current = null;
        Dao<T, ID> dao = getDao(updated.getClass());
        Field field = getFieldFromClass(fieldName, updated.getClass());
        if (field == null) throw new RuntimeException("Field not found");
        try {
            Field idField = getFieldFromClass("id", updated.getClass());
            List<T> existingOnes = dao.queryForEq(fieldName, field.get(updated));
            if (existingOnes.size() > 0) {
                current = existingOnes.get(0);
                idField.set(updated, idField.get(current));
//                dao.update(updated);
            } else {
                dao.create(updated);
                current = updated;
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return current;
    }

    public T update(T object) {
        Dao<T, ID> dao = getDao(object.getClass());
        try {
            dao.update(object);
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        return object;
    }

    public T refresh(T object) {
        Dao<T, ID> dao = getDao(object.getClass());
        try {
            dao.refresh(object);
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        return object;
    }
}
