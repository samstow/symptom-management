package com.samstow.symptommanagement.fragments;



import android.app.Fragment;
import android.view.View;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.samstow.symptommanagement.R;
import com.samstow.symptommanagement.event.QuestionResponseEvent;
import com.samstow.symptommanagement.event.bus.BusProvider;
import com.samstow.symptommanagement.model.QuestionImpl;
import com.samstow.symptommanagement.model.ResponseImpl;

import org.androidannotations.annotations.AfterExtras;
import org.androidannotations.annotations.AfterInject;
import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.CheckedChange;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.FragmentArg;
import org.androidannotations.annotations.UiThread;
import org.androidannotations.annotations.ViewById;

import java.util.List;

/**
 * A simple {@link android.app.Fragment} subclass.
 * Use the {@url https://github.com/excilys/androidannotations/wiki/FragmentArg} fragment builder method to
 * create an instance of this fragment.
 *
 */
@EFragment(R.layout.fragment_question_radio)
public class RadioQuestionFragment extends QuestionFragment implements RadioGroup.OnCheckedChangeListener {

    @ViewById(R.id.question)
    TextView questionText;
    @ViewById(R.id.radioGroup)
    RadioGroup radioGroup;
    @ViewById(R.id.pageLabel)
    TextView pageLabel;
    @ViewById(R.id.previous)
    Button previous;

    private String selectedResponse;
    private int questionIndex;

    public RadioQuestionFragment() {
        // Required empty public constructor
    }

    @AfterViews
    public void setupView() {

        questionText.setText(question.getQuestionText());

        radioGroup.removeAllViews();
        radioGroup.setOnCheckedChangeListener(this);
        List<String> responses = question.getResponseOptions();
        for (String response : responses) {
            RadioButton button = new RadioButton(getActivity());
            button.setText(response);
            button.setId(responses.indexOf(response));
            radioGroup.addView(button);
        }
        RadioButton button = (RadioButton)radioGroup.findViewById(0);
        button.setChecked(true);

        questionIndex = patient.getQuestionsByPriority().indexOf(question);
        int count = patient.getQuestions().size();
        pageLabel.setText(String.format("%d of %d", questionIndex+1, count));

        if (questionIndex == 0) {
            previous.setVisibility(View.GONE);
        } else {
            previous.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void onCheckedChanged(RadioGroup radioGroup, int i) {
        selectedResponse = question.getResponseOptions().get(i);
    }

    @Click(R.id.next)
    public void goToNextQuestion() {
        finishWithNextIndex(questionIndex + 1);
    }

    @Click(R.id.previous)
    public void goToPrevious() {
        finishWithNextIndex(questionIndex - 1);
    }

    @UiThread
    public void finishWithNextIndex(int index) {
        if (selectedResponse == null) {
            Toast.makeText(getActivity(), "Please select a response", Toast.LENGTH_SHORT).show();
            return;
        }

        ResponseImpl response = ResponseImpl.createRadioResponse(question.getQuestionText(), selectedResponse);
        QuestionResponseEvent event = new QuestionResponseEvent(response, index);
        BusProvider.getInstance().post(event);
    }


}
