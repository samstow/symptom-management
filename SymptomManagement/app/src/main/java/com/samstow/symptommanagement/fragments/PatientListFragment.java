package com.samstow.symptommanagement.fragments;

import android.app.Activity;
import android.app.Fragment;
import android.content.Context;
import android.os.Bundle;
import android.app.ListFragment;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;

import com.samstow.symptommanagement.R;

import com.samstow.symptommanagement.database.PersistenceManager;
import com.samstow.symptommanagement.event.PatientSelectedEvent;
import com.samstow.symptommanagement.event.bus.BusProvider;
import com.samstow.symptommanagement.model.DoctorImpl;
import com.samstow.symptommanagement.model.PatientImpl;
import com.samstow.symptommanagement.network.SymptomManagementService;
import com.squareup.otto.Subscribe;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Background;
import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.EditorAction;
import org.androidannotations.annotations.FocusChange;
import org.androidannotations.annotations.FragmentArg;
import org.androidannotations.annotations.UiThread;
import org.androidannotations.annotations.ViewById;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import retrofit.RetrofitError;


@EFragment(R.layout.search_list_fragment)
public class PatientListFragment extends ListFragment {


    @Bean
    SymptomManagementService service;

    @FragmentArg
    DoctorImpl doctor;

    @Bean
    PersistenceManager persistenceManager;

    @ViewById(R.id.searchText)
    EditText searchText;


    PatientAdapter listAdapter;
    List<PatientImpl> patients;

    /**
     * Mandatory empty constructor for the fragment manager to instantiate the
     * fragment (e.g. upon screen orientation changes).
     */
    public PatientListFragment() {
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();
    }

    private Object eventHandler = new Object() {


    };

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.search_list_fragment, container, false);
    }

    @AfterViews
    public void setUp() {
        persistenceManager.refresh(doctor);

        patients = new ArrayList<PatientImpl>(doctor.getPatients());
        listAdapter = new PatientAdapter(getActivity(),
                android.R.layout.simple_list_item_1, android.R.id.text1, patients);
        setListAdapter(listAdapter);

    }

    @EditorAction(R.id.searchText)
    public boolean onSearchAction(TextView v, int actionId, KeyEvent event) {
        if (actionId == EditorInfo.IME_ACTION_SEARCH ||
                actionId == EditorInfo.IME_ACTION_DONE ||
                actionId == EditorInfo.IME_ACTION_NEXT ||
                (event.getKeyCode() == android.view.KeyEvent.KEYCODE_ENTER)) {
            searchText.clearFocus();
            return true;
        }
        return false;
    }

    @FocusChange(R.id.searchText)
    public void onFocusChange(View v, boolean hasFocus) {
            if (!hasFocus) {
                // your action here
                InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(searchText.getWindowToken(), 0);
                performSearch(searchText.getText().toString());
            }
    }

    @Background
    public void performSearch(String searchText) {
        if (searchText == null || searchText.isEmpty()) {
            patients.clear();
            patients.addAll(doctor.getPatients());
            finishedSearch();
            return;
        }

        try {
            Collection<PatientImpl> foundPatients = (Collection)service.getServiceClient().findPatientsByUsername(searchText);
            patients.clear();
            patients.addAll(foundPatients);
            finishedSearch();
        } catch (RetrofitError e) {

        }
    }

    @UiThread
    public void finishedSearch() {
        listAdapter.notifyDataSetChanged();
    }



    @Override
    public void onListItemClick(ListView l, View v, int position, long id) {
        super.onListItemClick(l, v, position, id);
        PatientImpl patient = (PatientImpl) getListAdapter().getItem(position);
        BusProvider.getInstance().post(new PatientSelectedEvent(patient));
    }

    public static class PatientAdapter extends ArrayAdapter<PatientImpl> {

        private int resourceId;
        private Context context;
        private DateFormat dateFormat = SimpleDateFormat.getTimeInstance(DateFormat.SHORT);


        public PatientAdapter(Context context, int resource, int textViewResourceId, List<PatientImpl> objects) {
            super(context, resource, textViewResourceId, objects);
            setUp(context, resource);
        }

        private void setUp(Context context, int resource) {
            this.resourceId = resource;
            this.context = context;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            PatientImpl patient = getItem(position);
            View view = super.getView(position, convertView, parent);
            if (view instanceof TextView) {
                TextView textView = (TextView) view;
                String text = String.format("%s %s", patient.getFirstName(), patient.getLastName());
                textView.setText(text);
            }
            return view;
        }

    }
}
