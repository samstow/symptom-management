package com.samstow.symptommanagement.event;

import retrofit.client.Response;

/**
 * Created by samstow on 11/08/2014.
 */
public class SuccessEvent {
    private Object responseObject;
    private Response response;

    public SuccessEvent(Object responseObject, Response response) {
        this.responseObject = responseObject;
        this.response = response;
    }

    public Object getResponseObject() {
        return responseObject;
    }

    public Response getResponse() {
        return response;
    }
}
