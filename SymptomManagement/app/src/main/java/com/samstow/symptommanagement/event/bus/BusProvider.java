package com.samstow.symptommanagement.event.bus;

import com.squareup.otto.Bus;
import com.squareup.otto.ThreadEnforcer;

/**
 * Created by samstow on 19/10/2014.
 */
public final class BusProvider {

    private static final Bus BUS = new Bus();
    private static final Bus BUS_ASYNC = new Bus(ThreadEnforcer.ANY);

    public static Bus getInstance(){
        return BUS;
    }

    public static Bus getAsyncInstance() {
        return BUS_ASYNC;
    }

    private BusProvider() {
    }
}
