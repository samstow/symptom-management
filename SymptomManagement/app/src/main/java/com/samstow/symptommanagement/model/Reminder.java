package com.samstow.symptommanagement.model;

import android.text.format.DateUtils;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.j256.ormlite.field.DatabaseField;

import org.joda.time.LocalTime;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.List;

/**
 * Created by samstow on 12/11/14.
 */
public class Reminder implements Serializable, Comparable<Reminder> {

    //Native DB Id
    @JsonIgnore
    @DatabaseField(generatedId = true)
    private long id;

    @DatabaseField
    public Date time;

    @DatabaseField(foreign = true, foreignAutoRefresh = true)
    public PatientImpl patient;

    public Reminder() {}

    public long getId()
    {
        return id;
    }
    public Reminder(PatientImpl patient, Date time)
    {
        this.id = -1;
        this.patient = patient;
        this.time = time;
    }

    @Override
    public boolean equals(Object o) {

        if (o instanceof Reminder) {
            return id == ((Reminder) o).getId();
        }
        return super.equals(o);
    }

    public Calendar getCalender() {
        Calendar calendar = Calendar.getInstance();
        if (time != null) {
            calendar.setTime(time);
        }
        return calendar;
    }

    @Override
    public int compareTo(Reminder reminder) {
        if (reminder != null) {
            LocalTime localTime = LocalTime.fromDateFields(this.time);
            LocalTime otherLocalTime = LocalTime.fromDateFields(reminder.time);
            return localTime.compareTo(otherLocalTime);
        } else {
            return 0;
        }
    }

    /**
     * Generates the standard reminders for a patient at 8am, 12pm, 4pm and 8pm
     * @param patient
     * @return collection of reminders at specified times
     */
    public static Collection<Reminder> createStandardReminders(PatientImpl patient) {
        Calendar calendar = Calendar.getInstance();
        int currentHour = calendar.get(Calendar.HOUR_OF_DAY);
        int[] hoursOfDay = {8, 12, 16, 20};

        List<Reminder> reminders = new ArrayList<Reminder>();
        for (int i = 0; i < hoursOfDay.length; i++) {
            int hourOfDay = hoursOfDay[i];
            Date time = getNextTime(hourOfDay, 0);
            reminders.add(new Reminder(patient, time));
        }

        return reminders;
    }

    public Date getNextOccurrence() {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(this.time);
        return getNextTime(calendar);
    }

    public static Date getNextTime(int hourOfDay, int minute) {
        Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.HOUR_OF_DAY, hourOfDay);
        calendar.set(Calendar.MINUTE, minute);

        return getNextTime(calendar);
    }


    public static Date getNextTime(Calendar calendar) {
        Calendar now = Calendar.getInstance();

        if (now.after(calendar)) {
            calendar.roll(Calendar.DAY_OF_YEAR, true);
            if (calendar.get(Calendar.DAY_OF_YEAR) == 1) {
                calendar.roll(Calendar.YEAR, true);
            }
        }
        return calendar.getTime();
    }


}
