package com.samstow.symptommanagement.fragments;

import android.app.ListFragment;
import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

import com.samstow.symptommanagement.database.PersistenceManager;
import com.samstow.symptommanagement.event.CheckInSelectedEvent;
import com.samstow.symptommanagement.event.bus.BusProvider;
import com.samstow.symptommanagement.model.CheckIn;
import com.samstow.symptommanagement.model.CheckInImpl;
import com.samstow.symptommanagement.model.DoctorImpl;
import com.samstow.symptommanagement.model.PatientImpl;
import com.samstow.symptommanagement.network.SymptomManagementService;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.FragmentArg;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;


@EFragment
public class CheckInListFragment extends ListFragment {


    @Bean
    SymptomManagementService service;

    @FragmentArg
    CheckInImpl[] checkIns;

    @Bean
    PersistenceManager persistenceManager;

    /**
     * Mandatory empty constructor for the fragment manager to instantiate the
     * fragment (e.g. upon screen orientation changes).
     */
    public CheckInListFragment() {
    }

    @AfterViews
    public void setUp() {

        for (CheckInImpl checkIn : checkIns) {
            persistenceManager.refresh(checkIn);
        }

        List<CheckInImpl> checkInList = Arrays.asList(checkIns);
        Collections.sort(checkInList);
        setListAdapter(new CheckInAdapter(getActivity(),
                android.R.layout.simple_list_item_1, android.R.id.text1, checkInList));
    }


    @Override
    public void onListItemClick(ListView l, View v, int position, long id) {
        CheckInImpl checkIn = (CheckInImpl)getListAdapter().getItem(position);
        BusProvider.getInstance().post(new CheckInSelectedEvent(checkIn));
    }

    public static class CheckInAdapter extends ArrayAdapter<CheckInImpl> {

        private int resourceId;
        private Context context;
        private DateFormat dateFormat = DateFormat.getDateTimeInstance(DateFormat.SHORT, DateFormat.SHORT);


        public CheckInAdapter(Context context, int resource, int textViewResourceId, List<CheckInImpl> objects) {
            super(context, resource, textViewResourceId, objects);
            setUp(context, resource);
        }

        private void setUp(Context context, int resource) {
            this.resourceId = resource;
            this.context = context;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            CheckInImpl checkIn = getItem(position);
            View view = super.getView(position, convertView, parent);
            if (view instanceof TextView) {
                TextView textView = (TextView) view;
                String text = String.format("Check in at %s",
                        dateFormat.format(checkIn.getTime()));
                if (checkIn.getSubmittedDate() != null) {
                    text = text + (String.format(" (submitted %s)",
                            dateFormat.format(checkIn.getSubmittedDate())));
                }
                textView.setText(text);
            }
            return view;
        }

    }
}
