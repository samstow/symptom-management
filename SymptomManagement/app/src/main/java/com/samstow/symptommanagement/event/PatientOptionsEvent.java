package com.samstow.symptommanagement.event;

import com.samstow.symptommanagement.model.PatientImpl;

/**
 * Created by samstow on 28/11/14.
 */
public class PatientOptionsEvent {

    public enum PatientOption {
        CHECK_IN,
        SET_REMINDERS,
        UPDATE_DETAILS,
        VIEW_CHECK_INS,
        MEDICATIONS
    }

    public PatientOption optionSelected;
    public PatientImpl patient;

    public PatientOptionsEvent(PatientOption patientOption) {
        this(patientOption, null);
    }

    public PatientOptionsEvent(PatientOption patientOption, PatientImpl patient) {
        this.optionSelected = patientOption;
        this.patient = patient;
    }
}
