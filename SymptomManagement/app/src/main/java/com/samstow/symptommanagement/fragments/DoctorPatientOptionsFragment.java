package com.samstow.symptommanagement.fragments;

import android.app.Fragment;
import android.widget.TextView;

import com.samstow.symptommanagement.R;
import com.samstow.symptommanagement.event.PatientOptionsEvent;
import com.samstow.symptommanagement.event.bus.BusProvider;
import com.samstow.symptommanagement.model.PatientImpl;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.FragmentArg;
import org.androidannotations.annotations.ViewById;

@EFragment(R.layout.fragment_doctor_patient_options)
public class DoctorPatientOptionsFragment extends Fragment {

    @FragmentArg
    PatientImpl patient;

    @ViewById(R.id.txtPatientName)
    TextView txtPatientName;

    @AfterViews
    public void setPatientName() {

        String patientName = String.format("%s %s", patient.getFirstName(), patient.getLastName());
        txtPatientName.setText(patientName);
    }

    @Click(R.id.btn_check_ins)
    public void checkIn() {
        doOption(PatientOptionsEvent.PatientOption.VIEW_CHECK_INS);
    }

    @Click(R.id.btn_medications)
    public void setReminders() {
        doOption(PatientOptionsEvent.PatientOption.MEDICATIONS);
    }

    @Click(R.id.btn_details)
    public void updateDetails() {
        doOption(PatientOptionsEvent.PatientOption.UPDATE_DETAILS);
    }

    public void doOption(PatientOptionsEvent.PatientOption option) {
        BusProvider.getInstance().post(new PatientOptionsEvent(option, patient));
    }
}
