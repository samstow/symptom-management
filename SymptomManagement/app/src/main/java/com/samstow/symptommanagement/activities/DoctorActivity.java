package com.samstow.symptommanagement.activities;

import android.app.Activity;
import android.app.Fragment;
import android.util.Log;
import android.widget.Toast;

import com.samstow.symptommanagement.R;
import com.samstow.symptommanagement.database.PersistenceManager;
import com.samstow.symptommanagement.event.CheckInSelectedEvent;
import com.samstow.symptommanagement.event.PatientOptionsEvent;
import com.samstow.symptommanagement.event.PatientSelectedEvent;
import com.samstow.symptommanagement.event.PatientUpdateEvent;
import com.samstow.symptommanagement.event.bus.BusProvider;
import com.samstow.symptommanagement.fragments.CheckInListFragment_;
import com.samstow.symptommanagement.fragments.DoctorPatientOptionsFragment;
import com.samstow.symptommanagement.fragments.DoctorPatientOptionsFragment_;
import com.samstow.symptommanagement.fragments.MedicationsFragment_;
import com.samstow.symptommanagement.fragments.PatientDetailsFragment_;
import com.samstow.symptommanagement.fragments.PatientListFragment_;
import com.samstow.symptommanagement.fragments.ResponseListFragment_;
import com.samstow.symptommanagement.model.CheckInImpl;
import com.samstow.symptommanagement.model.DoctorImpl;
import com.samstow.symptommanagement.model.PatientImpl;
import com.samstow.symptommanagement.network.SymptomManagementService;
import com.squareup.otto.Subscribe;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Background;
import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.Extra;
import org.androidannotations.annotations.UiThread;

import java.util.Collection;

import retrofit.RetrofitError;

@EActivity(R.layout.activity_doctor)
public class DoctorActivity extends Activity {

    @Extra
    DoctorImpl doctor;

    @Bean
    SymptomManagementService service;

    @Bean
    PersistenceManager persistenceManager;

    @AfterViews
    public void showListFragment() {
       Fragment fragment =  PatientListFragment_.builder().doctor(doctor).build();
        getFragmentManager().beginTransaction().replace(R.id.container, fragment)
                .commit();
    }

    @Override
    protected void onResume() {
        super.onResume();
        BusProvider.getInstance().register(eventHandler);
    }

    @Override
    protected void onPause() {
        super.onPause();
        BusProvider.getInstance().unregister(eventHandler);
    }

    private Object eventHandler = new Object() {

        @Subscribe
        public void onPatientSelectedEvent(PatientSelectedEvent event) {
            showPatientOptions(event.patient);
        }

        @Subscribe
        public void onCheckInSelectedEvent(CheckInSelectedEvent event) {
            showResponses(event.checkIn);
        }

        @Subscribe
        public void onPatientOptionsEvent(PatientOptionsEvent event) {
            doOption(event);
        }

        @Subscribe
        public void onPatientUpdateEvent(PatientUpdateEvent event) {
            updatePatient(event.patient);
        }

    };

    public void doOption(PatientOptionsEvent event) {
        switch (event.optionSelected) {
            case VIEW_CHECK_INS:
                showCheckIns(event.patient);
                break;
            case MEDICATIONS:
                showMedications(event.patient);
                break;
            case UPDATE_DETAILS:
                startUpdateDetailsActivity(event.patient);
                break;
        }
    }

    public void startUpdateDetailsActivity(PatientImpl patient) {
        Fragment patientDetailsFragment = PatientDetailsFragment_.builder().patient(patient).build();
        addToBackStack(patientDetailsFragment);
    }

    public void showMedications(PatientImpl patient) {
        Fragment fragment = MedicationsFragment_.builder().patient(patient).build();
        addToBackStack(fragment);
    }

    @Background
    public void showPatientOptions(PatientImpl patient) {
        try {
            Collection<CheckInImpl> checkIns = (Collection)service.getServiceClient().getPatientCheckIns(patient.getServerId());
            persistenceManager.saveCheckIns(checkIns, patient);

        } catch (RetrofitError e) {
            serviceError(e);
        }
        Fragment fragment = DoctorPatientOptionsFragment_.builder().patient(patient).build();
        addToBackStack(fragment);
    }

    public void showCheckIns(PatientImpl patient) {
        showCheckIns(patient.getCheckIns());
    }

    public void showResponses(CheckInImpl checkIn) {
        Fragment fragment = ResponseListFragment_.builder().checkIn(checkIn).build();
        addToBackStack(fragment);
    }

    @UiThread
    public void showCheckIns(Collection<CheckInImpl> checkIns) {
        CheckInImpl[] checkInArray = new CheckInImpl[checkIns.size()];
        checkIns.toArray(checkInArray);
        Fragment fragment = CheckInListFragment_.builder().checkIns(checkInArray).build();
        addToBackStack(fragment);
    }

    @UiThread
    public void addToBackStack(Fragment fragment) {
        getFragmentManager().beginTransaction().replace(R.id.container, fragment).addToBackStack("").commit();
    }

    @Background
    public void updatePatient(PatientImpl patient) {
        PatientImpl updatedPatient = null;
        try {
            updatedPatient = (PatientImpl)service.getServiceClient().updatePatient(patient);
            persistenceManager.savePatient(patient);
            onPatientUpdateSuccess();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    @UiThread
    public void onPatientUpdateSuccess() {
        showToast("Patient details updated");
        getFragmentManager().popBackStack();
    }

    @UiThread
    public void serviceError(RetrofitError e) {
        Log.e(DoctorActivity.class.getName(), "Error ", e);
        showToast(e.getMessage());
    }

    private void showToast(String message) {
        Toast.makeText(
                this,
                message,
                Toast.LENGTH_SHORT).show();
    }
}
