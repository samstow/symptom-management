package com.samstow.symptommanagement.model;


import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.j256.ormlite.dao.EagerForeignCollection;
import com.j256.ormlite.dao.ForeignCollection;
import com.j256.ormlite.field.DataType;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.field.ForeignCollectionField;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.List;

/**
 * Created by samstow on 19/10/2014.
 */

@JsonIgnoreProperties(ignoreUnknown = true)
public class PatientImpl extends UserImpl implements Patient {

        @DatabaseField
        protected String patientRecordNumber;
        @DatabaseField
        protected Date dateOfBirth;
        @DatabaseField
        protected String doctorRegistrationId;

        protected List<QuestionImpl> questions;

        @JsonIgnore
        @DatabaseField(dataType = DataType.SERIALIZABLE)
        public ArrayList<Long> questionIds;
        @JsonIgnore
        @ForeignCollectionField(orderColumnName = "time", eager = true)
        protected Collection<Reminder> reminders;

        @JsonIgnore
        @ForeignCollectionField(orderColumnName = "time", orderAscending = false, eager = true)
        protected Collection<CheckInImpl> checkIns;

        @JsonIgnore
        @DatabaseField(foreign = true, foreignAutoRefresh = true)
        public DoctorImpl doctor;

        private static final String BASE_PASSWORD = "pass";

        public PatientImpl() {
            super();
            this.dateOfBirth = new Date();
            this.reminders = new ArrayList<Reminder>();
            this.checkIns = new ArrayList<CheckInImpl>();
            this.questions = new ArrayList<QuestionImpl>();
            this.questionIds = new ArrayList<Long>();
        }

        public PatientImpl(String userName, DoctorImpl doctor) {
            super();
            this.username = userName;
            this.password = BASE_PASSWORD;
            this.questionIds = new ArrayList<Long>();
            this.questions = new ArrayList<QuestionImpl>();
            setDoctor(doctor);
        }

        public List<QuestionImpl> getMedicationQuestions() {
            List<QuestionImpl> questionList = new ArrayList<QuestionImpl>();

            for (QuestionImpl question : questions) {
                if (question.getQuestionType() == QuestionType.MEDICATION) {
                    questionList.add(question);
                }
            }
            return questionList;
        }


    public static Collection<QuestionImpl> baseQuestions() {
            Collection<QuestionImpl> questions1 = new ArrayList<QuestionImpl>();
            QuestionImpl mouthPain = SymptomQuestionFactory.createSymptomQuestion("mouth pain / sore throat");
            QuestionImpl painMed = MedicationQuestonFactory.createGenericQuestion();
            QuestionImpl eatQuestion = SymptomQuestionFactory.createEatQuestion();
            painMed.setPriority(1);
            eatQuestion.setPriority(2);

            questions1.add(mouthPain);
            questions1.add(painMed);
            questions1.add(eatQuestion);

            return questions1;
        }

        public void setDoctor(DoctorImpl doctor) {
            this.doctor = doctor;
            this.doctorRegistrationId = doctor.getDoctorRegistrationId();
        }

        @Override
        public String getPatientRecordNumber() {
            return patientRecordNumber;
        }
        public void setPatientRecordNumber(String patientRecordNumber) {
            this.patientRecordNumber = patientRecordNumber;
        }
        @Override
        public Date getDateOfBirth() {
            if (dateOfBirth == null) {
                dateOfBirth = new Date();
            }
            return dateOfBirth;
        }
        public void setDateOfBirth(Date dateOfBirth) {
            this.dateOfBirth = dateOfBirth;
        }
        @Override
        public String getDoctorRegistrationId() {
            return doctorRegistrationId;
        }
        public void setDoctorRegistrationId(String doctorRegistrationId) {
            this.doctorRegistrationId = doctorRegistrationId;
        }
        @Override
        public List<QuestionImpl> getQuestions() {
            return questions;
        }
        public void setQuestions(List<QuestionImpl> questions) {
            this.questions = questions;
            if (questions != null) {
                syncQuestionIds();
            }
        }

        public List<Reminder> getReminders() {
            return new ArrayList<Reminder>(reminders);
        }

    public void setReminders(Collection<Reminder> reminders) {
        this.reminders = reminders;
    }

    private void syncQuestionIds() {
            if (questions == null) return;
            if (questionIds == null) {
                this.questionIds = new ArrayList<Long>();
            } else {
                this.questionIds.clear();
            }
            for (QuestionImpl question : questions) {
               questionIds.add(question.getAndroid_ID());
            }
        }

        public void addQuestion(QuestionImpl question) {
            this.questions.add(question);
            syncQuestionIds();
        }

        public void removeQuestion(QuestionImpl question) {
            this.questions.remove(question);
            syncQuestionIds();
        }

        public void addCheckIn(CheckInImpl checkIn) {
            this.checkIns.add(checkIn);
        }
        public void setCheckIns(Collection<CheckInImpl> checkIns) {
            this.checkIns = checkIns;
        }

        @Override
        public List<CheckInImpl> getCheckIns() {
            return new ArrayList<CheckInImpl>(checkIns);
        }

        public List<QuestionImpl> getQuestionsByPriority() {
            Collections.sort(questions);
            return questions;
        }

}
