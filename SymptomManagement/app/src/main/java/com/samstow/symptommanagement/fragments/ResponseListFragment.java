package com.samstow.symptommanagement.fragments;

import android.app.ListFragment;
import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

import com.samstow.symptommanagement.database.PersistenceManager;
import com.samstow.symptommanagement.model.CheckInImpl;
import com.samstow.symptommanagement.model.ResponseImpl;
import com.samstow.symptommanagement.network.SymptomManagementService;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.FragmentArg;

import java.text.DateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;


@EFragment
public class ResponseListFragment extends ListFragment {

    @FragmentArg
    CheckInImpl checkIn;

    @Bean
    PersistenceManager persistenceManager;

    /**
     * Mandatory empty constructor for the fragment manager to instantiate the
     * fragment (e.g. upon screen orientation changes).
     */
    public ResponseListFragment() {
    }

    @AfterViews
    public void setUp() {
        persistenceManager.refresh(checkIn);

        setListAdapter(new ResponseAdapter(getActivity(),
                android.R.layout.simple_list_item_1, android.R.id.text1, new ArrayList<ResponseImpl>(checkIn.getResponses())));
    }

    @Override
    public void onListItemClick(ListView l, View v, int position, long id) {
        super.onListItemClick(l, v, position, id);

    }

    public static class ResponseAdapter extends ArrayAdapter<ResponseImpl> {

        private int resourceId;
        private Context context;
        private DateFormat dateFormat = DateFormat.getDateTimeInstance(DateFormat.SHORT, DateFormat.SHORT);


        public ResponseAdapter(Context context, int resource, int textViewResourceId, List<ResponseImpl> objects) {
            super(context, resource, textViewResourceId, objects);
            setUp(context, resource);
        }

        private void setUp(Context context, int resource) {
            this.resourceId = resource;
            this.context = context;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            ResponseImpl response = getItem(position);
            View view = super.getView(position, convertView, parent);
            if (view instanceof TextView) {
                TextView textView = (TextView) view;
                String text = String.format("%s %s", response.getQuestion(), response.getTextRepresentation());
                textView.setText(text);
            }
            return view;
        }

    }
}
