package com.samstow.symptommanagement.fragments;



import android.os.Bundle;
import android.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.samstow.symptommanagement.R;
import com.samstow.symptommanagement.model.PatientImpl;
import com.samstow.symptommanagement.model.Question;
import com.samstow.symptommanagement.model.QuestionImpl;

import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.FragmentArg;

import java.util.Date;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@url https://github.com/excilys/androidannotations/wiki/FragmentArg} fragment builder method to
 * create an instance of this fragment.
 *
 */
@EFragment(R.layout.fragment_question)
public class QuestionFragment extends Fragment {

    @FragmentArg
    public QuestionImpl question;
    @FragmentArg
    public PatientImpl patient;

    public QuestionFragment() {
        // Required empty public constructor
    }





}
