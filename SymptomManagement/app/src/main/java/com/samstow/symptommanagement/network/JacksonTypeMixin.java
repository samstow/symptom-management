package com.samstow.symptommanagement.network;

import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonSubTypes.Type;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.samstow.symptommanagement.model.CheckInImpl;
import com.samstow.symptommanagement.model.DoctorImpl;
import com.samstow.symptommanagement.model.PatientImpl;
import com.samstow.symptommanagement.model.QuestionImpl;
import com.samstow.symptommanagement.model.ResponseImpl;
import com.samstow.symptommanagement.model.impl.DoctorBase;
import com.samstow.symptommanagement.model.impl.PatientBase;

public class JacksonTypeMixin {

	@JsonDeserialize(as = DoctorImpl.class)
	public abstract class DoctorMixin{}

    @JsonDeserialize(as = PatientImpl.class)
    public abstract class PatientMixin{}

    @JsonDeserialize(as= QuestionImpl.class)
    public abstract class QuestionMixIn{}

    @JsonDeserialize(as= ResponseImpl.class)
    public abstract class ResponseMixIn{}

    @JsonDeserialize(as= CheckInImpl.class)
    public abstract class CheckInMixIn{}

}
