package com.samstow.symptommanagement.model;

import com.j256.ormlite.field.DatabaseField;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created by samstow on 9/11/14.
 */
public class SymptomQuestionFactory {

    private static final String QUESTION_PREFIX = "How bad is your ";

    private static ArrayList<String> standardResponse() {
        ArrayList<String> responses = new ArrayList<String>();
        responses.add("Well controlled");
        responses.add("Moderate");
        responses.add("Severe");
        return responses;
    }

    static public QuestionImpl createEatQuestion() {
        String qText = "Does your pain stop you from eating / drinking?";
        String[] options = {"No", "Some", "I can't eat"};
        List<String> list = Arrays.asList(options);
        ArrayList<String> response = new ArrayList<String>(list);
        return new QuestionImpl(qText, response, ResponseType.RADIO_OPTION, QuestionType.SYMPTOM, "Eating pain");
    }

    static public QuestionImpl createSymptomQuestion(String symptomName) {
        return createSymptomQuestion(symptomName, 0);
    }

    static public QuestionImpl createSymptomQuestion(String symptomName, int priority) {
        String questionText = String.format("%s%s?", QUESTION_PREFIX, symptomName);
        QuestionImpl question = new QuestionImpl(questionText,
                standardResponse(),
                ResponseType.RADIO_OPTION,
                QuestionType.SYMPTOM,
                symptomName,
                priority);
        return question;
    }

    public void setSymptomName(String symptomName, QuestionImpl question) {
        question.setQuestionText(String.format("%s%s?", QUESTION_PREFIX, symptomName));
    }


}
