//package com.samstow.symptommanagement.database;
//
//import android.content.ContentProviderClient;
//import android.content.ContentProviderOperation;
//import android.content.ContentProviderResult;
//import android.content.ContentResolver;
//import android.content.ContentValues;
//import android.content.Context;
//import android.database.Cursor;
//import android.net.Uri;
//import android.os.RemoteException;
//import android.provider.BaseColumns;
//import android.util.Log;
//
//import com.optiisolutions.housekeeping.database.OptiiKeeperOrmContract;
//import com.optiisolutions.housekeeping.model.Asset;
//
//import java.lang.reflect.Field;
//import java.util.ArrayList;
//import java.util.Collections;
//import java.util.List;
//
///**
// * Created by samstow on 13/08/2014.
// */
//public class PatientContentManager {
//
//    private static final String TAG = PatientContentManager.class.getSimpleName();
//    private static String[] ASSET_COLUMNS_PROJECTION = {BaseColumns._ID, SymptomManagementContract.PatientContract.ASSET_ID, SymptomManagementContract.PatientContract.ASSET_NAME, SymptomManagementContract.PatientContract.ASSET_TREE_TYPE, SymptomManagementContract.PatientContract.PARENT_ASSET_ID};
//
//    public static Asset getAssetWithId(Context context, Integer assetId) {
//        Cursor cursor = getAssetCursorWithId(context.getContentResolver(), assetId);
//        List<Asset> assets = getAssetsFromCursor(cursor);
//        if (assets.size() == 1) {
//            return assets.get(0);
//        } else {
//            Log.d(TAG, assets.size() + " assets found");
//            return null;
//        }
//    }
//
//    private static Cursor getAssetCursorWithId(ContentResolver contentResolver, Integer assetId) {
//        String[] args = {String.valueOf(assetId)};
//        Cursor cursor = contentResolver.query(SymptomManagementContract.PatientContract.CONTENT_URI, ASSET_COLUMNS_PROJECTION, SymptomManagementContract.PatientContract.ASSET_ID + "= ?", args, null);
//        return cursor;
//    }
//
//    public static void logAssets(Context context) {
//
//        // ContentProviderClient test
//        ContentProviderClient client = context.getContentResolver().acquireContentProviderClient(SymptomManagementContract.PatientContract.CONTENT_URI);
//
//        Cursor c2 = null;
//
//        try {
//            c2 = client.query(SymptomManagementContract.PatientContract.CONTENT_URI, null, null, null, null);
//            c2.moveToFirst();
//            do {
//                for (int i = 0; i < c2.getColumnCount(); i++) {
//                    Log.d(TAG, c2.getColumnName(i) + " : " + c2.getString(i));
//                }
//            } while (c2.moveToNext());
//        } catch (RemoteException e) {
//            e.printStackTrace();
//        } finally {
//            if (c2 != null) {
//                c2.close();
//            }
//        }
//        client.release();
//    }
//
//    public static Uri saveAsset(Context context, Asset asset) {
//        // insert test
//        ContentValues values = getContentValues(asset);
//        Cursor cursor = getAssetCursorWithId(context.getContentResolver(), asset.AssetId);
//        if (cursor.getCount() > 0) {
//            return null;
//        } else {
//            return context.getContentResolver().insert(SymptomManagementContract.PatientContract.CONTENT_URI, values);
//        }
//    }
//
//    public static int saveAssetsBulk(Context context, List<Asset> assets) {
//        // bulkInsert test
//        ContentValues[] contentValues = new ContentValues[assets.size()];
//
//        Asset asset = null;
//        Asset previousAsset = null;
//        Collections.sort(assets);
//
//        for (int i = 0; i < assets.size(); i++) {
//            asset = assets.get(i);
//            if (i > 0) {
//                previousAsset = assets.get(i - 1);
//                if (asset.equals(previousAsset)) {
//                    Log.d(TAG, "skipping asset: " + asset);
//                    continue;
//                }
//            }
//            Log.d(TAG, String.format("Saving asset %s", asset));
//            contentValues[i] = getContentValues(asset);
//        }
//        return context.getContentResolver().bulkInsert(SymptomManagementContract.PatientContract.CONTENT_URI, contentValues);
//    }
//
//    public static ContentProviderResult[] saveAssetsBatch(Context context, List<Asset> assets) {
//        // applyBatch test
//        ArrayList<ContentProviderOperation> operations = new ArrayList<ContentProviderOperation>();
//
//        ContentProviderResult[] results = null;
//        Asset asset = null;
//        Asset previousAsset = null;
//        Collections.sort(assets);
//
//        for (int i = 0; i < assets.size(); i++) {
//            asset = assets.get(i);
//            if (i > 0) {
//                previousAsset = assets.get(i - 1);
//                if (asset.equals(previousAsset)) {
//                    Log.d(TAG, "skipping asset: " + asset);
//                    continue;
//                }
//            }
//            Log.d(TAG, String.format("Saving asset %s", asset));
//            operations.add(ContentProviderOperation.newInsert(SymptomManagementContract.PatientContract.CONTENT_URI).withValues(getContentValues(asset)).build());
//        }
//
//        try {
//            results = context.getContentResolver().applyBatch(SymptomManagementContract.AUTHORITY, operations);
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//        return results;
//    }
//
//    public static List<Asset> getAssets(Context context) {
//        // select test
//        Cursor c = context.getContentResolver().query(SymptomManagementContract.PatientContract.CONTENT_URI, null, null, null, null);
//        return getAssetsFromCursor(c);
//    }
//
//    public static int delete(Context context, Asset asset) {
//        String[] args = {String.valueOf(asset.AssetId)};
//        return context.getContentResolver().delete(SymptomManagementContract.PatientContract.CONTENT_URI, SymptomManagementContract.PatientContract.ASSET_ID + " = ?", args);
//    }
//
//    public static int deleteAllAssets(Context context) {
//        return context.getContentResolver().delete(SymptomManagementContract.PatientContract.CONTENT_URI, null, null);
//    }
//
//    private static List<Asset> getAssetsFromCursor(Cursor cursor) {
//        Field[] fields =  Asset.class.getFields();
//        if (cursor.getColumnCount() < 5) {
//            return null;
//        }
//        List<Asset> assets = new ArrayList<Asset>();
//        int baseIdIndex = cursor.getColumnIndex(BaseColumns._ID);
//        int idIndex = cursor.getColumnIndex(SymptomManagementContract.PatientContract.ASSET_ID);
//        int nameIndex = cursor.getColumnIndex(SymptomManagementContract.PatientContract.ASSET_NAME);
//        int treeTypeIndex = cursor.getColumnIndex(SymptomManagementContract.PatientContract.ASSET_TREE_TYPE);
//        int parentIdIndex = cursor.getColumnIndex(SymptomManagementContract.PatientContract.PARENT_ASSET_ID);
//        cursor.moveToFirst();
//        while (!cursor.isAfterLast()){
//            Asset asset = new Asset();
//            asset.id = cursor.getLong(baseIdIndex);
//            asset.AssetId = cursor.getInt(idIndex);
//            asset.AssetName = cursor.getString(nameIndex);
//            asset.AssetTreeType = cursor.getString(treeTypeIndex);
//            asset.ParentAssetId = cursor.getInt(parentIdIndex);
//            assets.add(asset);
//            cursor.moveToNext();
//        }
//        cursor.close();
//        return assets;
//    }
//
//    private static ContentValues getContentValues(Asset Asset) {
//        ContentValues contentValues = new ContentValues();
//        contentValues.put(SymptomManagementContract.PatientContract.ASSET_ID, Asset.AssetId);
//        contentValues.put(SymptomManagementContract.PatientContract.ASSET_NAME, Asset.AssetName);
//        contentValues.put(SymptomManagementContract.PatientContract.ASSET_TREE_TYPE, Asset.AssetTreeType);
//        contentValues.put(SymptomManagementContract.PatientContract.PARENT_ASSET_ID, Asset.ParentAssetId);
//        return contentValues;
//    }
//}
