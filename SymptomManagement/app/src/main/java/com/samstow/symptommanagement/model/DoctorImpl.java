package com.samstow.symptommanagement.model;

import com.j256.ormlite.dao.ForeignCollection;
import com.j256.ormlite.field.ForeignCollectionField;
import com.samstow.symptommanagement.model.impl.DoctorBase;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 * Created by samstow on 19/10/2014.
 */
public class DoctorImpl extends UserImpl implements Doctor {

    @ForeignCollectionField(eager = true)
    private Collection<PatientImpl> patients;

    public DoctorImpl() {
        super();
        this.patients = new ArrayList<PatientImpl>();
    }

    @Override
    public String getDoctorRegistrationId() {
        return getServerId();
    }


    @Override
    public Collection<PatientImpl> getPatients() {
        return patients;
    }

}
