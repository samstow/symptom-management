package com.samstow.symptommanagement.fragments;

import android.app.AlertDialog;
import android.app.ListFragment;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.samstow.symptommanagement.R;
import com.samstow.symptommanagement.database.PersistenceManager;
import com.samstow.symptommanagement.event.CheckInSelectedEvent;
import com.samstow.symptommanagement.event.PatientUpdateEvent;
import com.samstow.symptommanagement.event.bus.BusProvider;
import com.samstow.symptommanagement.model.CheckInImpl;
import com.samstow.symptommanagement.model.MedicationQuestonFactory;
import com.samstow.symptommanagement.model.PatientImpl;
import com.samstow.symptommanagement.model.Question;
import com.samstow.symptommanagement.model.QuestionImpl;
import com.samstow.symptommanagement.network.SymptomManagementService;
import com.samstow.symptommanagement.utils.SwipeDismissListViewTouchListener;
import com.samstow.symptommanagement.utils.SwipeDismissTouchListener;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.FragmentArg;
import org.androidannotations.annotations.OptionsItem;
import org.androidannotations.annotations.OptionsMenu;
import org.androidannotations.annotations.OptionsMenuItem;
import org.androidannotations.annotations.ViewById;

import java.sql.SQLException;
import java.text.DateFormat;
import java.util.Arrays;
import java.util.List;


@EFragment
@OptionsMenu(R.menu.doctor)
public class MedicationsFragment extends ListFragment {

    @Bean
    SymptomManagementService service;

    @FragmentArg
    PatientImpl patient;

    @Bean
    PersistenceManager persistenceManager;


    QuestionAdapter questionAdapter;
    List<QuestionImpl> questions;

    ListView listView;
    SwipeDismissTouchListener swipeDismissTouchListener;

    /**
     * Mandatory empty constructor for the fragment manager to instantiate the
     * fragment (e.g. upon screen orientation changes).
     */
    public MedicationsFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.list_fragment_medications, container, false);
    }

    @Click(R.id.btnUpdate)
    public void update() {
        BusProvider.getInstance().post(new PatientUpdateEvent(patient, false));
    }

    @AfterViews
    public void setUp() {
//        setHasOptionsMenu(true);
        persistenceManager.refresh(patient);
        try {
            persistenceManager.loadPatientQuestions(patient);
        } catch (SQLException ex) {
            ex.printStackTrace();
        }

        questions = patient.getMedicationQuestions();
        questionAdapter = new QuestionAdapter(getActivity(),
                android.R.layout.simple_list_item_1, android.R.id.text1, questions);
        setListAdapter(questionAdapter);

        listView = getListView();

        // Create a ListView-specific touch listener. ListViews are given special treatment because
        // by default they handle touches for their list items... i.e. they're in charge of drawing
        // the pressed state (the list selector), handling list item clicks, etc.
        SwipeDismissListViewTouchListener touchListener =
                new SwipeDismissListViewTouchListener(
                        listView,
                        new SwipeDismissListViewTouchListener.OnDismissCallback() {
                            @Override
                            public void onDismiss(ListView listView, int[] reverseSortedPositions) {
                                    for (int position : reverseSortedPositions) {
                                        removeQuestionAtPosition(position);
                                    }
                                refreshQuestions();
                            }
                        });
        listView.setOnTouchListener(touchListener);
        // Setting this scroll listener is required to ensure that during ListView scrolling,
        // we don't look for swipes.
        listView.setOnScrollListener(touchListener.makeScrollListener());

    }

    @OptionsItem(R.id.add)
    public void addMedication() {

        final EditText input = new EditText(getActivity());
        AlertDialog alert = new AlertDialog.Builder(getActivity())
                .setTitle("Enter medication name")
                .setView(input)
                .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {
                        String medication = input.getText().toString();
                        addMedication(medication);
                    }
                })
                .setNegativeButton("Cancel", null).show();

    }

    public void removeQuestionAtPosition(int position) {
        int count = questions.size();
        if (count == 1 && MedicationQuestonFactory.isGenericMedQuestion(questions.get(0))) {
            Toast.makeText(getActivity(), "Cannot remove only medication", Toast.LENGTH_SHORT).show();
            return;
        }
        else if (count == 1) {
            questionAdapter.clear();
            questionAdapter.add(MedicationQuestonFactory.createGenericQuestion());
        }
        else {
            questionAdapter.remove(questionAdapter.getItem(position));
        }
    }

    public void addMedication(String medication) {
        QuestionImpl question = MedicationQuestonFactory.createMedicationQuestion(medication);
        if (questions.size() == 1 && MedicationQuestonFactory.isGenericMedQuestion(questions.get(0))) {
            questions.clear();
        }
        questions.add(question);
        refreshQuestions();
    }

    public void refreshQuestions() {
        persistenceManager.resetPatientMedQuestions(questions, patient);
        questionAdapter.notifyDataSetChanged();
    }


    @Override
    public void onListItemClick(ListView l, View v, int position, long id) {
        CheckInImpl checkIn = (CheckInImpl)getListAdapter().getItem(position);
        BusProvider.getInstance().post(new CheckInSelectedEvent(checkIn));
    }

    public static class QuestionAdapter extends ArrayAdapter<QuestionImpl> {

        private int resourceId;
        private Context context;
        private DateFormat dateFormat = DateFormat.getDateTimeInstance(DateFormat.SHORT, DateFormat.SHORT);


        public QuestionAdapter(Context context, int resource, int textViewResourceId, List<QuestionImpl> objects) {
            super(context, resource, textViewResourceId, objects);
            setUp(context, resource);
        }

        private void setUp(Context context, int resource) {
            this.resourceId = resource;
            this.context = context;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            QuestionImpl question = getItem(position);
            View view = super.getView(position, convertView, parent);
            if (view instanceof TextView) {
                TextView textView = (TextView) view;
                textView.setText(question.getQuestionInfo());
            }
            return view;
        }

    }
}
