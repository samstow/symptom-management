package com.samstow.symptommanagement.activities;

import android.app.Activity;
import android.content.Intent;
import android.util.Log;
import android.view.Window;
import android.widget.EditText;
import android.widget.Toast;

import com.j256.ormlite.dao.Dao;
import com.samstow.symptommanagement.R;
import com.samstow.symptommanagement.client.UserSvcApi;
import com.samstow.symptommanagement.database.PersistenceManager;
import com.samstow.symptommanagement.event.FailureEvent;
import com.samstow.symptommanagement.event.bus.BusProvider;
import com.samstow.symptommanagement.model.CheckIn;
import com.samstow.symptommanagement.model.CheckInImpl;
import com.samstow.symptommanagement.model.DoctorImpl;
import com.samstow.symptommanagement.model.Patient;
import com.samstow.symptommanagement.model.PatientImpl;
import com.samstow.symptommanagement.model.Reminder;
import com.samstow.symptommanagement.network.SymptomManagementService;
import com.samstow.symptommanagement.reminders.AlarmReceiver_;
import com.samstow.symptommanagement.reminders.AlarmService;
import com.samstow.symptommanagement.reminders.AlarmService_;
import com.samstow.symptommanagement.reminders.Constants;
import com.squareup.otto.Subscribe;

import org.androidannotations.annotations.Background;
import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.Fullscreen;
import org.androidannotations.annotations.UiThread;
import org.androidannotations.annotations.ViewById;
import org.androidannotations.annotations.WindowFeature;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import retrofit.RetrofitError;


@EActivity(R.layout.activity_login_screen)
public class LoginActivity extends Activity {

    private final static String TAG= LoginActivity.class.getSimpleName();

    @ViewById
	protected EditText username;
    @ViewById
	protected EditText password;
    @ViewById
	protected EditText server;

    @Bean
    SymptomManagementService service;
    @Bean
    PersistenceManager persistenceManager;


    @Override
    protected void onResume() {
        super.onResume();
        BusProvider.getInstance().register(eventhandler);
    }

    @Override
    protected void onPause() {
        super.onPause();
        BusProvider.getInstance().unregister(eventhandler);
    }

    @Click(R.id.loginButton)
	public void login() {
		String user = username.getText().toString();
		String pass = password.getText().toString();
		String serverStr = server.getText().toString();
        service.makeClient(user, pass);
        getRoles();
	}

    @Background
    public void getRoles() {
        List<String> roles = null;
        try {
            roles = service.getServiceClient().getUserRoles();
        } catch (Exception e) {
            //Exception Handling by retrofit
            return;
        }
        if (roles != null) {
            getRolesSuccess(roles);
        }
    }

    @UiThread
    public void getRolesSuccess(List<String> roles) {
        showToast(String.format("Roles: %s", roles));
        getDetails(roles);
    }

    @Background
    public void getDetails(List<String> roles) {
        if (roles.contains(UserSvcApi.ROLE_DOCTOR)) {
            configureDoctor();
        } else if (roles.contains(UserSvcApi.ROLE_PATIENT)) {
            configurePatient();
        }
    }

    @Background
    public void configureDoctor() {
        DoctorImpl doctor = null;
        DoctorImpl currentDoctor = null;
        try {
            doctor = (DoctorImpl)service.getServiceClient().getMyDetailsDoctor();
            doctor.setPassword(password.getText().toString());
            currentDoctor = persistenceManager.getStoredDoctorFromResponse(doctor);

            Collection<? extends Patient> myPatients = service.getServiceClient().getPatients();
            for (Patient patient : myPatients) {
                PatientImpl currentPatient = persistenceManager.getStoredPatientFromResponse((PatientImpl)patient);
                if (currentPatient.doctor == null) {
                    persistenceManager.ensureDoctorForPatient(currentPatient, currentDoctor);
                }
                if (currentPatient.getQuestions() == null || currentPatient.getQuestions().size() == 0) {
                    currentPatient = persistenceManager.addBaseQuestions(currentPatient);
                }
            }
            persistenceManager.refresh(currentDoctor);
            startDoctorActivity(currentDoctor);

        } catch (RetrofitError ex) {
            ex.printStackTrace();
        } catch (SQLException ex) {
            ex.printStackTrace();
        }

    }

    @UiThread
    public void startDoctorActivity(DoctorImpl doctor) {
        DoctorActivity_.intent(this).doctor(doctor).start();
    }

    @Background
    public void configurePatient() {
        PatientImpl currentPatient = null;
        try {
            PatientImpl patient = (PatientImpl) service.getServiceClient().getMyDetailsPatient();

            patient.setPassword(password.getText().toString());
            currentPatient = persistenceManager.getStoredPatientFromResponse(patient);
            if (currentPatient.doctor == null) {
                DoctorImpl doctor = (DoctorImpl)service.getServiceClient().getDoctor();
                persistenceManager.ensureDoctorForPatient(currentPatient, doctor);
            }
            if (currentPatient.getQuestions() == null || currentPatient.getQuestions().size() == 0) {
                currentPatient = persistenceManager.addBaseQuestions(currentPatient);
                service.getServiceClient().updateMyDetails(currentPatient);
            }
            if (currentPatient.getReminders() == null || currentPatient.getReminders().size() == 0) {
                Collection<Reminder> reminders = Reminder.createStandardReminders(currentPatient);
                persistenceManager.saveReminders(reminders, currentPatient);
                persistenceManager.refresh(currentPatient);
                Intent intent = AlarmService.makeIntent(currentPatient, null);
                AlarmService_.intent(this).createForPatient(intent).start();
            }

            Reminder reminder = currentPatient.getReminders().get(0);
            Intent checkInIntent = new Intent(this, AlarmReceiver_.class);
            checkInIntent.setAction(AlarmReceiver_.ACTION_ON_RECEIVE_ACTION);
            checkInIntent.putExtra(Constants.EXTRA_PATIENT, patient);
            checkInIntent.putExtra(Constants.EXTRA_REMINDER, reminder);
            sendBroadcast(checkInIntent);

        } catch (RetrofitError ex) {
            ex.printStackTrace();
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        //TODO: show options fragment (enables selection of check in or edit reminders, etc.
//        startCheckInActivity(currentPatient);
        if (currentPatient != null) {
            startPatientActivity(currentPatient);
        }
    }

    @UiThread
    public void startPatientActivity(PatientImpl patient) {
        PatientActivity_.intent(this).patient(patient).start();
    }



    private final Object eventhandler = new Object() {

        @Subscribe
        public void onFailureEvent(FailureEvent event) {
            loginError(event.getRetrofitError());
        }
    };

    private void loginError(RetrofitError e) {
        Log.e(LoginActivity.class.getName(), "Error logging in via OAuth.", e);

        showToast(e.getMessage());
    }

    private void showToast(String message) {
        Toast.makeText(
                LoginActivity.this,
                message,
                Toast.LENGTH_SHORT).show();
    }

}
