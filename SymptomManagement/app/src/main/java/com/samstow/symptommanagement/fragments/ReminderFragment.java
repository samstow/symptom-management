package com.samstow.symptommanagement.fragments;

import android.app.Activity;
import android.app.TimePickerDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.app.ListFragment;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.TimePicker;

import com.j256.ormlite.dao.Dao;
import com.samstow.symptommanagement.R;

import com.samstow.symptommanagement.database.PersistenceManager;
import com.samstow.symptommanagement.database.SymptomManagementHelper;
import com.samstow.symptommanagement.model.PatientImpl;
import com.samstow.symptommanagement.model.Reminder;
import com.samstow.symptommanagement.reminders.AlarmService;
import com.samstow.symptommanagement.reminders.AlarmService_;

import org.androidannotations.annotations.AfterInject;
import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.FragmentArg;
import org.androidannotations.annotations.ItemClick;
import org.androidannotations.annotations.OrmLiteDao;

import java.sql.SQLException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

@EFragment
public class ReminderFragment extends ListFragment {


    @FragmentArg
    public PatientImpl patient;

    @Bean
    public PersistenceManager persistenceManager;

    @OrmLiteDao(helper = SymptomManagementHelper.class, model = Reminder.class)
    public Dao<Reminder, Long> reminderDao;

    private List<Reminder> reminders;

    /**
     * Mandatory empty constructor for the fragment manager to instantiate the
     * fragment (e.g. upon screen orientation changes).
     */
    public ReminderFragment() {
    }

        //TODO: add options menu to add reminders


    @AfterInject
    public void ensureReminders() {
        persistenceManager.refresh(patient);
        reminders = new ArrayList<Reminder>(patient.getReminders());
        Collections.sort(reminders);
    }

    @AfterViews
    public void setUp() {
        setListAdapter(new ReminderAdapter(getActivity(),
                android.R.layout.simple_list_item_1, android.R.id.text1, reminders));
    }


    @Override
    public void onListItemClick(ListView l, View v, int position, long id) {
        final Reminder reminder = reminders.get(position);
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(reminder.time);
        final int hourOfDay = calendar.get(Calendar.HOUR_OF_DAY);
        int minute = calendar.get(Calendar.MINUTE);

        TimePickerDialog timePickerDialog = new TimePickerDialog(getActivity(), new TimePickerDialog.OnTimeSetListener() {
            @Override
            public void onTimeSet(TimePicker timePicker, int hour, int newMinute) {
                reminder.time = Reminder.getNextTime(hour, newMinute);
                try {
                    reminderDao.update(reminder);
                    Intent intent = AlarmService.makeIntent(patient, reminder);
                    AlarmService_.intent(getActivity()).reschedule(intent).start();
                    BaseAdapter adapter = (BaseAdapter)ReminderFragment.this.getListAdapter();
                    adapter.notifyDataSetChanged();

                } catch (SQLException ex) {
                    ex.printStackTrace();
                }
            }
        }, hourOfDay, minute, false);

        timePickerDialog.show();
    }

    public static class ReminderAdapter extends ArrayAdapter<Reminder> {

        private int resourceId;
        private Context context;
        private DateFormat dateFormat = SimpleDateFormat.getTimeInstance(DateFormat.SHORT);


        public ReminderAdapter(Context context, int resource, int textViewResourceId, List<Reminder> objects) {
            super(context, resource, textViewResourceId, objects);
            setUp(context, resource);
        }

        private void setUp(Context context, int resource) {
            this.resourceId = resource;
            this.context = context;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            Reminder reminder = getItem(position);
            View view = super.getView(position, convertView, parent);
            if (view instanceof TextView) {
                TextView textView = (TextView) view;
                String text = dateFormat.format(reminder.time);
                textView.setText(text);
            }
            return view;
        }

    }
}
