package com.samstow.symptommanagement.event;

import com.samstow.symptommanagement.model.Response;
import com.samstow.symptommanagement.model.ResponseImpl;

/**
 * Created by samstow on 5/11/14.
 */
public class QuestionResponseEvent {

    private ResponseImpl response;
    private int nextQuestionIndex;

    public QuestionResponseEvent(ResponseImpl response, int nextQuestionIndex) {
        this.response = response;
        this.nextQuestionIndex = nextQuestionIndex;
    }

    public ResponseImpl getResponse() {
        return response;
    }

    public int getNextQuestionIndex() {
        return nextQuestionIndex;
    }

}
