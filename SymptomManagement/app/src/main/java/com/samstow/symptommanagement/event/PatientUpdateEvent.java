package com.samstow.symptommanagement.event;

import com.samstow.symptommanagement.model.PatientImpl;

/**
 * Created by samstow on 29/11/14.
 */
public class PatientUpdateEvent {

    public boolean isNewPatient;
    public PatientImpl patient;

    public PatientUpdateEvent(PatientImpl patient, boolean isNewPatient) {
        this.isNewPatient = isNewPatient;
        this.patient = patient;
    }
}
