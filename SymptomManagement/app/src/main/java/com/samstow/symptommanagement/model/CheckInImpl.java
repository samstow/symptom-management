package com.samstow.symptommanagement.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.j256.ormlite.dao.BaseForeignCollection;
import com.j256.ormlite.dao.ForeignCollection;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.field.ForeignCollectionField;

import java.io.Serializable;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.List;

/**
 * Created by samstow on 19/10/2014.
 */
public class CheckInImpl implements CheckIn, Serializable, Comparable<CheckInImpl> {

    @JsonIgnore
    @DatabaseField(foreign = true)
    private PatientImpl patient;
    //Native DB Id
    @JsonIgnore
    @DatabaseField(generatedId = true)
    private long id;
    @JsonIgnore
    @DatabaseField
    private boolean isComplete;

    //Server Id
    @JsonProperty("id")
    @DatabaseField
    protected String serverId;
    @ForeignCollectionField(eager = true)
    private Collection<ResponseImpl> responses;
    @DatabaseField
    private Date time;
    @DatabaseField
    private Date submittedDate;

    public CheckInImpl() {
        this.id = -1;
        this.time = Calendar.getInstance().getTime();
    }

    public CheckInImpl(PatientImpl patient) {
        this.id = -1;
        this.time = Calendar.getInstance().getTime();
        this.patient = patient;
    }

    public long getAndroid_ID() {
        return id;
    }

    @Override
    public String getId() {
        return serverId;
    }

    @Override
    public Date getTime() {
        return time;
    }

    @Override
    public Collection<ResponseImpl> getResponses() {
        return responses;
    }

    @Override
    public Date getSubmittedDate() {
        return submittedDate;
    }

    public PatientImpl getPatient() {
        return patient;
    }

    public void setPatient(PatientImpl patient) {
        this.patient = patient;
    }

    public void setResponses(Collection<ResponseImpl> responses) {
        this.responses = responses;
    }

    public void addResponse(ResponseImpl response) {
        this.responses.add(response);
    }

    public boolean isComplete() {
        return isComplete;
    }

    public void setComplete(boolean isComplete) {
        this.isComplete = isComplete;
    }

    public void setSubmittedDate(Date submittedDate) {
        this.submittedDate = submittedDate;
    }


    @Override
    public boolean equals(Object o) {
        if (o instanceof CheckInImpl) {
            CheckInImpl other = (CheckInImpl) o;
            return this.time.equals(other.getTime());
        } else {
            return super.equals(o);
        }
    }

    @Override
    public int compareTo(CheckInImpl checkIn) {
        return checkIn.time.compareTo(this.time);
    }
}
