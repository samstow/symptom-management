package com.samstow.symptommanagement.fragments;



import android.app.DatePickerDialog;
import android.os.Bundle;
import android.app.Fragment;
import android.text.Editable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.DatePicker;
import android.widget.EditText;

import com.samstow.symptommanagement.R;
import com.samstow.symptommanagement.database.PersistenceManager;
import com.samstow.symptommanagement.event.PatientUpdateEvent;
import com.samstow.symptommanagement.event.bus.BusProvider;
import com.samstow.symptommanagement.model.PatientImpl;

import org.androidannotations.annotations.AfterTextChange;
import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.BeforeTextChange;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.FocusChange;
import org.androidannotations.annotations.FragmentArg;
import org.androidannotations.annotations.ViewById;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;


@EFragment(R.layout.fragment_patient_details)
public class PatientDetailsFragment extends Fragment {


    @FragmentArg
    PatientImpl patient;

    @Bean
    PersistenceManager persistenceManager;

    @ViewById
    EditText txtPassword;
    @ViewById
    EditText txtUsername;
    @ViewById
    EditText txtFirstName;
    @ViewById
    EditText txtLastName;
    @ViewById
    EditText txtPhone;
    @ViewById
    EditText txtDOB;

    private boolean isNewPatient = false;
    private Calendar calendar = Calendar.getInstance();
    private DateFormat dateFormat = DateFormat.getDateInstance(DateFormat.MEDIUM, Locale.ENGLISH);

    public PatientDetailsFragment() {
        // Required empty public constructor
    }

    @AfterViews
    public void setUp() {
        if (patient == null) {
            patient = new PatientImpl();
            isNewPatient = true;
        } else {
            persistenceManager.refresh(patient);
        }
        txtUsername.setText(patient.getUsername());
        txtPassword.setText(patient.getPassword());
        txtFirstName.setText(patient.getFirstName());
        txtLastName.setText(patient.getLastName());
        txtPhone.setText(patient.getPhone());
        calendar.setTime(patient.getDateOfBirth());
        updateDOB();
//        txtDOB.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                updateDate();
//            }
//        });
    }

    private void updateDOB() {
        String dob = dateFormat.format(patient.getDateOfBirth());
        txtDOB.setText(dob);
    }

    @Click(R.id.btnUpdate)
    public void updatePatient() {
        BusProvider.getInstance().post(new PatientUpdateEvent(patient, isNewPatient));
    }

    @AfterTextChange(R.id.txtUsername)
    public void updateUsername(Editable text) {
        patient.setUsername(text.toString());
    }

    @AfterTextChange(R.id.txtPassword)
    public void updatePassword(Editable text) {
        patient.setPassword(text.toString());
    }

    @AfterTextChange(R.id.txtFirstName)
    public void updateFirstname(Editable text) {
        patient.setFirstName(text.toString());
    }

    @AfterTextChange(R.id.txtLastName)
    public void updateLastname(Editable text) {
        patient.setLastName(text.toString());
    }

    @AfterTextChange(R.id.txtPhone)
    public void updatePhone(Editable text) {
        patient.setPhone(text.toString());
    }

    @Click(R.id.txtDOB)
    public void updateDate() {
        new DatePickerDialog(getActivity(), onDateSetListener, calendar
                .get(Calendar.YEAR), calendar.get(Calendar.MONTH),
                calendar.get(Calendar.DAY_OF_MONTH)).show();
    }

    private DatePickerDialog.OnDateSetListener onDateSetListener = new DatePickerDialog.OnDateSetListener() {
        @Override
        public void onDateSet(DatePicker datePicker, int year, int monthOfYear, int dayOfMonth) {
            calendar.set(Calendar.YEAR, year);
            calendar.set(Calendar.MONTH, monthOfYear);
            calendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
            patient.setDateOfBirth(calendar.getTime());
            updateDOB();
        }
    };

}
