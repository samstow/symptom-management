package com.samstow.symptommanagement.database;

import android.content.Context;
import java.sql.SQLException;
import android.database.sqlite.SQLiteDatabase;

import com.j256.ormlite.android.apptools.OrmLiteSqliteOpenHelper;
import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.support.ConnectionSource;
import com.j256.ormlite.table.TableUtils;
import com.samstow.symptommanagement.model.CheckInImpl;
import com.samstow.symptommanagement.model.DoctorImpl;
import com.samstow.symptommanagement.model.PatientImpl;
import com.samstow.symptommanagement.model.QuestionImpl;
import com.samstow.symptommanagement.model.Reminder;
import com.samstow.symptommanagement.model.ResponseImpl;

/**
 * Created by samstow on 13/08/2014.
 */
public class SymptomManagementHelper extends OrmLiteSqliteOpenHelper {
    // name of the database file for your application -- change to something appropriate for your app
    private static final String DATABASE_NAME = "symptomManagement.db";
    // any time you make changes to your database objects, you may have to increase the database version
    private static final int DATABASE_VERSION = 1;

    public SymptomManagementHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    /**
     * Returns the Database Access Object (DAO) for our SimpleData class. It will create it or just give the cached
     * value.
     */
//    public Dao<Class, Integer> getDao(Class clazz) throws SQLException {
//        return getDao(clazz);
//    }

    @Override
    public void onCreate(SQLiteDatabase database, ConnectionSource connectionSource) {
        try {
            createTables(connectionSource);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    private void createTables(ConnectionSource connectionSource) throws SQLException {
        TableUtils.createTableIfNotExists(connectionSource, PatientImpl.class);
        TableUtils.createTableIfNotExists(connectionSource, DoctorImpl.class);
        TableUtils.createTableIfNotExists(connectionSource, QuestionImpl.class);
        TableUtils.createTableIfNotExists(connectionSource, ResponseImpl.class);
        TableUtils.createTableIfNotExists(connectionSource, CheckInImpl.class);
        TableUtils.createTableIfNotExists(connectionSource, Reminder.class);
    }

    @Override
    public void onUpgrade(SQLiteDatabase database, ConnectionSource connectionSource, int oldVersion, int newVersion) {
        try {
            TableUtils.dropTable(connectionSource, PatientImpl.class, true);
            TableUtils.dropTable(connectionSource, DoctorImpl.class, true);
            TableUtils.dropTable(connectionSource, QuestionImpl.class, true);
            TableUtils.dropTable(connectionSource, ResponseImpl.class, true);
            TableUtils.dropTable(connectionSource, CheckInImpl.class, true);
            TableUtils.dropTable(connectionSource, Reminder.class, true);

            createTables(connectionSource);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}