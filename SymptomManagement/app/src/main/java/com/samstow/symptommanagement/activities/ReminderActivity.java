package com.samstow.symptommanagement.activities;

import android.app.Activity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import com.samstow.symptommanagement.R;
import com.samstow.symptommanagement.database.PersistenceManager;
import com.samstow.symptommanagement.fragments.ReminderFragment;
import com.samstow.symptommanagement.fragments.ReminderFragment_;
import com.samstow.symptommanagement.model.PatientImpl;
import com.samstow.symptommanagement.model.Reminder;

import org.androidannotations.annotations.AfterExtras;
import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.Extra;

@EActivity(R.layout.activity_reminder)
public class ReminderActivity extends Activity {

    @Extra
    public PatientImpl patient;

    @Bean
    public PersistenceManager persistenceManager;

    @AfterExtras
    public void showFragment() {
        if (patient != null) {
            persistenceManager.refresh(patient);
        }
        ReminderFragment reminderFragment = ReminderFragment_.builder().patient(patient).build();
        getFragmentManager().beginTransaction().replace(R.id.container, reminderFragment).commit();
    }

}
