//package com.samstow.symptommanagement.database;
//
//import com.optiisolutions.housekeeping.model.Asset;
//import com.tojc.ormlite.android.OrmLiteSimpleContentProvider;
//import com.tojc.ormlite.android.framework.MatcherController;
//import com.tojc.ormlite.android.framework.MimeTypeVnd;
//
///**
// * Created by samstow on 13/08/2014.
// */
//public class SymptomManagementProvider extends OrmLiteSimpleContentProvider<SymptomManagementHelper> {
//@Override
//protected Class<SymptomManagementHelper> getHelperClass() {
//        return SymptomManagementHelper.class;
//}
//
//@Override
//public boolean onCreate() {
//        setMatcherController(new MatcherController()//
//        .add(Asset.class, MimeTypeVnd.SubType.DIRECTORY, "", SymptomManagementContract.PatientContract.CONTENT_URI_PATTERN_MANY)//
//        .add(Asset.class, MimeTypeVnd.SubType.ITEM, "#", SymptomManagementContract.PatientContract.CONTENT_URI_PATTERN_ONE));
//        return true;
//        }
//        }
