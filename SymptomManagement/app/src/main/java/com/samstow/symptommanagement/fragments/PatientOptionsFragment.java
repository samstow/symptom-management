package com.samstow.symptommanagement.fragments;

import android.app.Activity;
import android.net.Uri;
import android.os.Bundle;
import android.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.samstow.symptommanagement.R;
import com.samstow.symptommanagement.event.PatientOptionsEvent;
import com.samstow.symptommanagement.event.bus.BusProvider;

import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EFragment;

@EFragment(R.layout.fragment_patient_options)
public class PatientOptionsFragment extends Fragment {


    @Click(R.id.btn_check_in)
    public void checkIn() {
        doOption(PatientOptionsEvent.PatientOption.CHECK_IN);
    }

    @Click(R.id.btn_reminders)
    public void setReminders() {
        doOption(PatientOptionsEvent.PatientOption.SET_REMINDERS);
    }

    @Click(R.id.btn_details)
    public void updateDetails() {
        doOption(PatientOptionsEvent.PatientOption.UPDATE_DETAILS);
    }

    public void doOption(PatientOptionsEvent.PatientOption option) {
        BusProvider.getInstance().post(new PatientOptionsEvent(option));
    }
}
