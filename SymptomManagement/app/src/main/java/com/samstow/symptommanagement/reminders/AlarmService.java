package com.samstow.symptommanagement.reminders;

import android.app.AlarmManager;
import android.app.IntentService;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.database.Cursor;

import com.j256.ormlite.dao.Dao;
import com.samstow.symptommanagement.activities.CheckInActivity_;
import com.samstow.symptommanagement.database.PersistenceManager;
import com.samstow.symptommanagement.database.SymptomManagementHelper;
import com.samstow.symptommanagement.model.Patient;
import com.samstow.symptommanagement.model.PatientImpl;
import com.samstow.symptommanagement.model.Reminder;

import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.EIntentService;
import org.androidannotations.annotations.OrmLiteDao;
import org.androidannotations.annotations.ServiceAction;
import org.androidannotations.annotations.SystemService;
import org.androidannotations.annotations.res.StringRes;

import java.sql.SQLException;

@EIntentService
public class AlarmService extends IntentService {

    private static final String TAG = "AlarmService";

    public static final String CREATE_ALL = "CreateAll";
    public static final String CREATE_FOR_PATIENT = "CreateForPatient";
    public static final String CANCEL_FOR_PATIENT = "CancelForPatient";
    public static final String SCHEDULE_NEXT = "ScheduleNext";
    public static final String RESCHEDULE = "Reschedule";

    @OrmLiteDao(helper = SymptomManagementHelper.class, model = PatientImpl.class)
    Dao<PatientImpl, Long> patientDao;

    @OrmLiteDao(helper = SymptomManagementHelper.class, model = Reminder.class)
    Dao<Reminder, Long> reminderDao;

    @Bean
    PersistenceManager persistenceManager;

    @SystemService
    AlarmManager alarmManager;

    public AlarmService() {
        super(TAG);
    }

//    static public void process(Context context, PatientImpl patient, Reminder reminder, String action) {
//        Intent service = makeIntent(context, patient, reminder, action);
//        context.startService(service);
//    }

    static public Intent makeIntent(PatientImpl patient, Reminder reminder) {
        Intent service = new Intent();
        if (patient != null) {
            service.putExtra(Constants.EXTRA_PATIENT, patient);
        }
        if (reminder != null) {
            service.putExtra(Constants.EXTRA_REMINDER, reminder);
        }
        return service;
    }

    @ServiceAction(value = CREATE_ALL)
    void createAll(Intent intent) {
        for (PatientImpl patient : patientDao) {
            setAlarmsForPatient(patient);
        }
    }

    @ServiceAction(value = CREATE_FOR_PATIENT)
    void createForPatient(Intent intent) {
        handleIntent(intent, CREATE_FOR_PATIENT);
    }

    @ServiceAction(value = CANCEL_FOR_PATIENT)
    void cancelForPatient(Intent intent) {
        handleIntent(intent, CANCEL_FOR_PATIENT);
    }

    @ServiceAction(value = SCHEDULE_NEXT)
    void scheduleNext(Intent intent) {
        PatientImpl patient = (PatientImpl)intent.getSerializableExtra(Constants.EXTRA_PATIENT);
        Reminder reminder = (Reminder)intent.getSerializableExtra(Constants.EXTRA_REMINDER);
        if (patient == null || reminder == null) return;

        cancelAlarm(patient, reminder);
        reminder.time = reminder.getNextOccurrence();
        try {
            reminderDao.update(reminder);
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        setAlarm(patient, reminder);
    }

    @ServiceAction(value = RESCHEDULE)
    void reschedule(Intent intent) {
        PatientImpl patient = (PatientImpl)intent.getSerializableExtra(Constants.EXTRA_PATIENT);
        Reminder reminder = (Reminder)intent.getSerializableExtra(Constants.EXTRA_REMINDER);
        if (patient == null || reminder == null) return;

        cancelAlarm(patient, reminder);
        setAlarm(patient, reminder);
    }

    private void handleIntent(Intent intent, String action) {
        PatientImpl patient = (PatientImpl)intent.getSerializableExtra(Constants.EXTRA_PATIENT);
        Reminder reminder = (Reminder)intent.getSerializableExtra(Constants.EXTRA_REMINDER);


        if (patient == null) return;
        persistenceManager.refresh(patient);

        if (reminder == null) {
            if (CREATE_FOR_PATIENT.equals(action)) {
                setAlarmsForPatient(patient);
            } else if (CANCEL_FOR_PATIENT.equals(action)) {
                cancelAlarmsForPatient(patient);
            }
        } else if (reminder != null) {
            persistenceManager.refresh(reminder);
            if (CREATE_FOR_PATIENT.equals(action)) {
                setAlarm(patient, reminder);
            } else if (CANCEL_FOR_PATIENT.equals(action)) {
                cancelAlarm(patient, reminder);
            }
        }
    }

    private void setAlarmsForPatient(PatientImpl patient) {
        for (Reminder reminder : patient.getReminders()) {
            setAlarm(patient, reminder);
        }
    }

    private void cancelAlarmsForPatient(PatientImpl patient) {
        for (Reminder reminder : patient.getReminders()) {
            cancelAlarm(patient, reminder);
        }
    }

    private void setAlarm(PatientImpl patient, Reminder reminder)
    {
        PendingIntent pi = getPendingIntent(patient, reminder);
        alarmManager.set(AlarmManager.RTC_WAKEUP, reminder.time.getTime(), pi);
    }

    private void cancelAlarm(PatientImpl patient, Reminder reminder) {
        PendingIntent pi = getPendingIntent(patient, reminder);
        alarmManager.cancel(pi);
    }

    public PendingIntent getPendingIntent (PatientImpl patient, Reminder reminder) {
        return AlarmService.getPendingIntent(patient, reminder, this);
    }

    static public PendingIntent getPendingIntent (PatientImpl patient, Reminder reminder, Context context) {
        Intent checkInIntent = new Intent(context, AlarmReceiver_.class);
        checkInIntent.setAction(AlarmReceiver_.ACTION_ON_RECEIVE_ACTION);
        checkInIntent.putExtra(Constants.EXTRA_PATIENT, patient);
        checkInIntent.putExtra(Constants.EXTRA_REMINDER, reminder);
        PendingIntent pi = PendingIntent.getBroadcast(context, 0, checkInIntent, PendingIntent.FLAG_UPDATE_CURRENT);
        return pi;
    }

    @Override
    protected void onHandleIntent(Intent intent) {

    }

}


