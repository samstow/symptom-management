package com.samstow.symptommanagement.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.j256.ormlite.field.DataType;
import com.j256.ormlite.field.DatabaseField;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by samstow on 6/11/14.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class UserImpl implements User, Serializable {

    //Native DB Id
    @JsonIgnore
    @DatabaseField(generatedId = true)
    private long id;

    @DatabaseField
    protected String serverId;
    @DatabaseField
    protected String username = "";
    @DatabaseField
    protected String password = "";
    @DatabaseField
    protected String firstName = "";
    @DatabaseField
    protected String lastName = "";
    @DatabaseField
    protected String phone = "";
    @DatabaseField(dataType = DataType.SERIALIZABLE)
    protected ArrayList<String> roles;

    @Override
    public boolean equals(Object o) {

        if (o instanceof UserImpl) {
            UserImpl other = (UserImpl) o;
            return (other.getId() == getId() && getId() != 0) || (getServerId() != null && getServerId().equals(other.getServerId()));
        } else {
            return false;
        }
    }

    public UserImpl() {
        this.roles = new ArrayList<String>();
    }

    public long getId() {
        return id;
    }


    public String getServerId() {
        return serverId;
    }

    public void setServerId(String id) {
        this.serverId = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    @Override
    public List<String> getRoles() {
        return roles;
    }

}