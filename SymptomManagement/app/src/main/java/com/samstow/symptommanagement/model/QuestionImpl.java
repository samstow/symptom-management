package com.samstow.symptommanagement.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.j256.ormlite.dao.ForeignCollection;
import com.j256.ormlite.field.DataType;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.field.ForeignCollectionField;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;

/**
 * Created by samstow on 8/11/14.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class QuestionImpl implements Serializable, Question, Comparable<QuestionImpl> {

    //Native DB Id
    @JsonIgnore
    @DatabaseField(generatedId = true)
    private long id;

    public long getAndroid_ID() {
        return id;
    }

    @DatabaseField
    protected String serverId;
    @DatabaseField(uniqueIndex = true)
    private String questionText;
    @DatabaseField(dataType = DataType.SERIALIZABLE)
    ArrayList<String> responseOptions = new ArrayList<String>();
    @DatabaseField
    int priority;
    @DatabaseField
    private ResponseType responseType;
    @DatabaseField
    private QuestionType questionType;
    @DatabaseField
    private String questionInfo;


    public QuestionImpl() {
    }


    public QuestionImpl(String text, ArrayList<String> responseOptions, ResponseType responseType, QuestionType questionType, String questionInfo) {
        this(text, responseOptions, responseType, questionType, questionInfo, 0);
    }

    public QuestionImpl(String text, ResponseType responseType, QuestionType questionType, String questionInfo, int priority) {
        this.questionText = text;
        this.responseType = responseType;
        this.priority = priority;
        this.questionType = questionType;
        this.questionInfo = questionInfo;
    }

    public QuestionImpl(String text, ArrayList<String> responseOptions, ResponseType responseType, QuestionType questionType, String questionInfo, int priority) {
        this.questionText = text;
        this.responseOptions = responseOptions;
        this.responseType = responseType;
        this.priority = priority;
        this.questionType = questionType;
        this.questionInfo = questionInfo;
    }

    @Override
    public String getServerId() {
        return serverId;
    }

    public void setServerId(String serverId) {
        this.serverId = serverId;
    }

    @Override
    public String getQuestionText() {
        return questionText;
    }

    public void setQuestionText(String text) {
        this.questionText = text;
    }

    @Override
    public List<String> getResponseOptions() {
        return responseOptions;
    }

    @Override
    public int getPriority() {
        return priority;
    }

    @Override
    public ResponseType getResponseType() {
        return responseType;
    }

    @Override
    public QuestionType getQuestionType() {
        return questionType;
    }

    @Override
    public String getQuestionInfo() {
        return questionInfo;
    }

    public void setResponseOptions(ArrayList<String> responseOptions) {
        this.responseOptions = responseOptions;
    }

    public void setPriority(int priority) {
        this.priority = priority;
    }

    public void setResponseType(ResponseType responseType) {
        this.responseType = responseType;
    }

    @Override
    public int compareTo(QuestionImpl question) {
        Integer thisPriority = priority;
        Integer thatPriority = question.getPriority();
        return thisPriority.compareTo(thatPriority);
    }

    @Override
    public boolean equals(Object o) {
        if (o instanceof QuestionImpl) {
            QuestionImpl q = (QuestionImpl)o;
            return q.getQuestionText().equals(questionText);
        }
        return super.equals(o);
    }
}
