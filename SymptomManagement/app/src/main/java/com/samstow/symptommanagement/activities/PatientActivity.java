package com.samstow.symptommanagement.activities;

import android.app.Activity;
import android.app.Fragment;
import android.app.FragmentTransaction;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

import com.samstow.symptommanagement.R;
import com.samstow.symptommanagement.database.PersistenceManager;
import com.samstow.symptommanagement.event.FailureEvent;
import com.samstow.symptommanagement.event.PatientOptionsEvent;
import com.samstow.symptommanagement.event.PatientUpdateEvent;
import com.samstow.symptommanagement.event.bus.BusProvider;
import com.samstow.symptommanagement.fragments.PatientDetailsFragment_;
import com.samstow.symptommanagement.fragments.PatientOptionsFragment;
import com.samstow.symptommanagement.fragments.PatientOptionsFragment_;
import com.samstow.symptommanagement.model.CheckInImpl;
import com.samstow.symptommanagement.model.PatientImpl;
import com.samstow.symptommanagement.network.SymptomManagementService;
import com.squareup.otto.Subscribe;

import org.androidannotations.annotations.AfterExtras;
import org.androidannotations.annotations.Background;
import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.Extra;
import org.androidannotations.annotations.UiThread;

import retrofit.RetrofitError;

@EActivity(R.layout.activity_patient)
public class PatientActivity extends Activity {

    private static final String TAG = PatientActivity.class.getSimpleName();

    @Extra
    PatientImpl patient;

    @Bean
    SymptomManagementService service;
    @Bean
    PersistenceManager persistenceManager;


    @AfterExtras
    public void showPatientOptionsFragment() {
        Fragment fragment = PatientOptionsFragment_.builder().build();
        replaceFragment(fragment);
    }

    public void replaceFragment(Fragment fragment) {
        getFragmentManager().beginTransaction()
                .replace(R.id.container, fragment)
                .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN)
                .commit();
    }
    public void addFragment(Fragment fragment) {
        getFragmentManager().beginTransaction()
                .replace(R.id.container, fragment)
                .addToBackStack("")
                .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN)
                .commit();
    }

    @Override
    protected void onResume() {
        super.onResume();
        BusProvider.getInstance().register(eventhandler);
    }

    @Override
    protected void onPause() {
        super.onPause();
        BusProvider.getInstance().unregister(eventhandler);
    }

    public void doOption(PatientOptionsEvent.PatientOption option) {
        switch (option) {
            case CHECK_IN:
                startCheckInActivity();
                break;
            case SET_REMINDERS:
                startReminderActivity();
                break;
            case UPDATE_DETAILS:
                startUpdateDetailsActivity();
                break;
        }
    }

    public void startCheckInActivity() {
        CheckInActivity_.intent(this).patient(patient).start();
    }

    public void startReminderActivity() {
        ReminderActivity_.intent(this).patient(patient).start();
    }

    public void startUpdateDetailsActivity() {
        Fragment patientDetailsFragment = PatientDetailsFragment_.builder().patient(patient).build();
        addFragment(patientDetailsFragment);
    }

    private final Object eventhandler = new Object() {

        @Subscribe
        public void onPatientOptionsEvent(PatientOptionsEvent event) {
            doOption(event.optionSelected);
        }

        @Subscribe
        public void onPatientUpdateEvent(PatientUpdateEvent event) {
            if (!event.isNewPatient) {
                updatePatient(event.patient);
            }
        }

        @Subscribe
        public void onFailureEvent(FailureEvent event) {
            serverError(event.getRetrofitError());
        }
    };

    @Background
    public void updatePatient(PatientImpl patient) {
        PatientImpl updatedPatient = null;
        try {
            updatedPatient = (PatientImpl)service.getServiceClient().updateMyDetails(patient);
            persistenceManager.savePatient(patient);
            onPatientUpdateSuccess();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    @UiThread
    public void onPatientUpdateSuccess() {
        showToast("Patient details updated");
        getFragmentManager().popBackStack();
    }

    private void serverError(RetrofitError e) {
        Log.e(TAG, "Error", e);

        showToast(e.getMessage());
    }

    private void showToast(String message) {
        Toast.makeText(
                this,
                message,
                Toast.LENGTH_SHORT).show();
    }
}
