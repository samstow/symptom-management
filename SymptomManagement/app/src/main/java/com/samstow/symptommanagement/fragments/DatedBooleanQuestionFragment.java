package com.samstow.symptommanagement.fragments;



import android.app.TimePickerDialog;
import android.view.View;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.samstow.symptommanagement.R;
import com.samstow.symptommanagement.event.QuestionResponseEvent;
import com.samstow.symptommanagement.event.bus.BusProvider;
import com.samstow.symptommanagement.model.ResponseImpl;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.UiThread;
import org.androidannotations.annotations.ViewById;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;

/**
 * A simple {@link android.app.Fragment} subclass.
 * Use the {@url https://github.com/excilys/androidannotations/wiki/FragmentArg} fragment builder method to
 * create an instance of this fragment.
 *
 */
@EFragment(R.layout.fragment_question_radio)
public class DatedBooleanQuestionFragment extends QuestionFragment implements RadioGroup.OnCheckedChangeListener {

    @ViewById(R.id.question)
    TextView questionText;
    @ViewById(R.id.radioGroup)
    RadioGroup radioGroup;
    @ViewById(R.id.pageLabel)
    TextView pageLabel;
    @ViewById(R.id.previous)
    Button previous;

    private List<String> responses;
    private String selectedResponse;
    private int questionIndex;
    private Date yesTime;
    private RadioButton yesButton;

    static String YES = "Yes";
    static String NO = "No";
    static String[] shortResponses = {YES, NO};

    public DatedBooleanQuestionFragment() {
        // Required empty public constructor
    }

    @AfterViews
    public void setupView() {

        questionText.setText(question.getQuestionText());

        radioGroup.removeAllViews();
        radioGroup.setOnCheckedChangeListener(this);
        responses = Arrays.asList(shortResponses);

        for (String response : responses) {
            RadioButton button = new RadioButton(getActivity());
            button.setText(response);
            button.setId(responses.indexOf(response));
            radioGroup.addView(button);
        }
        yesButton = (RadioButton)radioGroup.findViewById(0);

        questionIndex = patient.getQuestionsByPriority().indexOf(question);
        int count = patient.getQuestions().size();
        pageLabel.setText(String.format("%d of %d", questionIndex+1, count));

        if (questionIndex == 0) {
            previous.setVisibility(View.GONE);
        } else {
            previous.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void onCheckedChanged(RadioGroup radioGroup, int i) {
        selectedResponse = responses.get(i);
        if (selectedResponse.equals(YES)) {
            showTimePicker();
        } else {
            yesButton.setText(YES);
            yesTime = null;
        }
    }

    private void showTimePicker() {

        Calendar date = Calendar.getInstance();
        int hours = date.get(Calendar.HOUR_OF_DAY);
        int minutes = date.get(Calendar.MINUTE);
        TimePickerDialog tpd = new TimePickerDialog(getActivity(),
                new TimePickerDialog.OnTimeSetListener() {

                    @Override
                    public void onTimeSet(TimePicker view, int hourOfDay,
                                          int minute) {
                        Calendar calendar = Calendar.getInstance();
                        calendar.set(Calendar.HOUR_OF_DAY, hourOfDay);
                        calendar.set(Calendar.MINUTE, minute);
                        yesTime = calendar.getTime();

                        DateFormat dateFormat = new SimpleDateFormat("K:mm a");
                        String time = dateFormat.format(yesTime);

                        yesButton.setText(String.format("%s (%s)", YES, time));
                    }
                }, hours, minutes, false);
        tpd.show();
    }

    @Click(R.id.next)
    public void goToNextQuestion() {
        finishWithNextIndex(questionIndex + 1);
    }

    @Click(R.id.previous)
    public void goToPrevious() {
        finishWithNextIndex(questionIndex - 1);
    }

    @UiThread
    public void finishWithNextIndex(int index) {
        if (selectedResponse == null) {
            Toast.makeText(getActivity(), "Please select a response", Toast.LENGTH_SHORT).show();
            return;
        }

        ResponseImpl response = ResponseImpl.createDatedBooleanResponse(question.getQuestionText(), selectedResponse.equals(YES), yesTime);
        QuestionResponseEvent event = new QuestionResponseEvent(response, index);
        BusProvider.getInstance().post(event);
    }


}
