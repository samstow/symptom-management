package com.samstow.symptommanagement.database;

import android.content.Context;

import com.j256.ormlite.android.apptools.OpenHelperManager;
import com.j256.ormlite.dao.Dao;
import com.samstow.symptommanagement.model.CheckInImpl;
import com.samstow.symptommanagement.model.DoctorImpl;
import com.samstow.symptommanagement.model.PatientImpl;
import com.samstow.symptommanagement.model.Question;
import com.samstow.symptommanagement.model.QuestionImpl;
import com.samstow.symptommanagement.model.Reminder;
import com.samstow.symptommanagement.model.Response;
import com.samstow.symptommanagement.model.ResponseImpl;

import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.EBean;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

/**
 * Created by samstow on 9/11/14.
 */
@EBean
public class PersistenceManager {

    private Context context;
    @Bean
    PersistenceHelper<DoctorImpl, Long> doctorDaoHelper;
    @Bean
    PersistenceHelper<PatientImpl, Long> patientDaoHelper;
    @Bean
    PersistenceHelper<QuestionImpl, Long> questionDaoHelper;
    @Bean
    PersistenceHelper<CheckInImpl, Long> checkInDaoHelper;
    @Bean
    PersistenceHelper<ResponseImpl, Long> responseDaoHelper;
    @Bean
    PersistenceHelper<Reminder, Long> reminderHelper;

    private SymptomManagementHelper symptomManagementHelper;


    public PersistenceManager(Context context) {
        this.context = context;
        symptomManagementHelper = OpenHelperManager.getHelper(context, SymptomManagementHelper.class);
    }

    public Object getObject(String field, Object value, Class clazz) {
        Dao dao = getDao(clazz);
        try {
            return dao.queryBuilder().where().eq(field, value).queryForFirst();
        } catch (SQLException e) {
            return null;
        }
    }

    public void refresh(Object object) {
        Dao dao = getDao(object.getClass());
        try {
            dao.refresh(object);
            dao.update(object);
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
    }


    public CheckInImpl createCheckIn(PatientImpl patient) {
        CheckInImpl checkIn = new CheckInImpl();
        checkInDaoHelper.createOrRetrieve("id", checkIn);
        patient.addCheckIn(checkIn);
        patientDaoHelper.update(patient);
        checkInDaoHelper.refresh(checkIn);

        Dao<CheckInImpl, Long> dao = getDao(CheckInImpl.class);
        Dao<ResponseImpl, Long> responseDao = getDao(ResponseImpl.class);
        if (checkIn.getResponses() == null) {
            try {
                dao.assignEmptyForeignCollection(checkIn, "responses");
                int numResponses = checkIn.getResponses().size();
                if (numResponses > 0) {
                    List<ResponseImpl> responses = new ArrayList<ResponseImpl>(checkIn.getResponses());
                    for (int i = 0; i < numResponses; i++) {
                        ResponseImpl response = responses.get(i);
                        responseDao.delete(response);
                        checkIn.getResponses().remove(response);
                    }
                }
            } catch (SQLException ex) {
                ex.printStackTrace();
            }
        }
        saveCheckIn(checkIn);
        savePatient(patient);
        return checkIn;
    }

    public void saveCheckIns(Collection<CheckInImpl> checkIns, PatientImpl patient) {
        for (CheckInImpl checkIn : checkIns) {
            if (patient.getCheckIns().contains(checkIn)) return;

            if (checkIn.getAndroid_ID() == -1) {
                checkInDaoHelper.createOrRetrieve("id", checkIn);
            }
            Collection<ResponseImpl> responses = checkIn.getResponses();
            if (checkIn.getPatient() == null) {
                patient.addCheckIn(checkIn);
                patientDaoHelper.update(patient);
                checkInDaoHelper.refresh(checkIn);
            }
            for (ResponseImpl response : responses) {
                handleResponseForCheckIn(response, checkIn);
            }

            if (checkIn.getPatient() == null) {
                checkIn.setPatient(patient);
            }
            saveCheckIn(checkIn);
        }
    }

    public void savePatient(PatientImpl patient) {
        if (patient.getCheckIns() != null) {
            for (CheckInImpl checkIn : patient.getCheckIns()) {
                saveCheckIn(checkIn);
            }
        }
        patientDaoHelper.update(patient);
    }

    public void saveCheckIn(CheckInImpl checkIn) {
        checkInDaoHelper.update(checkIn);
        if (checkIn.getResponses() != null) {
            for (ResponseImpl response : checkIn.getResponses()) {
                saveResponse(response, checkIn);
            }
        }
        checkInDaoHelper.refresh(checkIn);
        patientDaoHelper.refresh(checkIn.getPatient());
    }

    public void saveReminders(Collection<Reminder> reminders, PatientImpl patient) {
        if (patient.getReminders() == null) {
            try {
                patientDaoHelper.getDao(PatientImpl.class).assignEmptyForeignCollection(patient, "reminders");
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        for (Reminder reminder : reminders) {
            Reminder reminder1 = reminderHelper.createOrRetrieve("id", reminder);
            if (!patient.getReminders().contains(reminder1)) {
                patient.getReminders().add(reminder1);
            }
        }
        patientDaoHelper.update(patient);
    }

    public void saveResponse(ResponseImpl response, CheckInImpl checkIn) {
        if (response.getAndroid_ID() == -1) {
            responseDaoHelper.createOrRetrieve("id", response);
            response.setCheckIn(checkIn);
        }

        responseDaoHelper.update(response);
    }

    public PatientImpl addBaseQuestions(PatientImpl patient) {
        Collection<QuestionImpl> questions = PatientImpl.baseQuestions();
        return syncQuestions(patient, questions);
    }

    public PatientImpl syncQuestions(PatientImpl patient, Collection<QuestionImpl> questions) {
        Dao<PatientImpl, Long> patientLongDao = patientDaoHelper.getDao(PatientImpl.class);
        try {
            questions = checkPersisted(questions);
            if (patient.getQuestions() == null) {
                patient.setQuestions(new ArrayList<QuestionImpl>());
            }
            for (QuestionImpl question : questions) {
                if (!patient.getQuestions().contains(question)) {
                    patient.addQuestion(question);
                }
            }
            patientLongDao.update(patient);
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        return patient;
    }

    public Collection<QuestionImpl> checkPersisted(Collection<QuestionImpl> questions) throws SQLException {
        if (questions == null || questions.size() == 0) return questions;
        Collection<QuestionImpl> persisted = new ArrayList<QuestionImpl>();

        for (QuestionImpl question : questions) {
            QuestionImpl persistedQ = questionDaoHelper.createOrRetrieve("questionText", question);
            if (persistedQ.getServerId() == null && question.getServerId() != null) {
                persistedQ.setServerId(question.getServerId());
                questionDaoHelper.update(persistedQ);
            }
            persisted.add(persistedQ);
        }
        return persisted;
    }

    public void resetPatientMedQuestions(List<QuestionImpl> questions, PatientImpl patient) {
        List<QuestionImpl> medQuestions = patient.getMedicationQuestions();
        try {
            checkPersisted(questions);
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        for (QuestionImpl question : medQuestions) {
            if (!questions.contains(question)) {
                patient.removeQuestion(question);
            }
        }
        for (QuestionImpl question : questions) {
            if (!patient.getQuestions().contains(question)) {
                patient.addQuestion(question);
            }
        }
        patientDaoHelper.update(patient);
    }

    public void handleResponseForCheckIn(ResponseImpl thisResponse, CheckInImpl checkIn) {
        boolean responseHandled = false;
        List<ResponseImpl> responses = new ArrayList<ResponseImpl>(checkIn.getResponses());

        for (ResponseImpl response : responses) {
            if (thisResponse.getQuestion() != null && thisResponse.getQuestion().equals(response.getQuestion())) {
                responseHandled = true;
                updateResponse(response, thisResponse);
            }
        }
        if (!responseHandled) {
            saveResponse(thisResponse, checkIn);
        }
        saveCheckIn(checkIn);
    }

    public void updateResponse(ResponseImpl existing, ResponseImpl updated) {
        switch (existing.getResponseType()) {
            case RADIO_OPTION:
            case FREE_TEXT:
                existing.setResponseText(updated.getResponseText());
                break;
            case DATED_BOOLEAN:
                existing.setResponse(updated.getResponse());
                existing.setYesTime(updated.getYesTime());
                break;
            case BOOLEAN:
                existing.setResponse(updated.getResponse());
                break;
            case CHECKED_OPTIONS:
                //TODO
                break;
        }
    }

    public DoctorImpl getStoredDoctorFromResponse(DoctorImpl doctor) throws SQLException {

        DoctorImpl currentDoctor = doctorDaoHelper.createOrRetrieve(doctor);
        for (PatientImpl patient : doctor.getPatients()) {
            PatientImpl currentPatient = getStoredPatientFromResponse(patient);
            if (currentPatient.doctor == null) {
                currentPatient.doctor = currentDoctor;
                patientDaoHelper.getDao(PatientImpl.class).update(currentPatient);
            }
        }
        doctorDaoHelper.getDao(DoctorImpl.class).refresh(currentDoctor);
        return currentDoctor;
    }

    public PatientImpl getStoredPatientFromResponse(PatientImpl patient) throws SQLException {
        PatientImpl currentPatient = patientDaoHelper.createOrRetrieve(patient);
        Dao<PatientImpl, Long> patientDao = getDao(PatientImpl.class);

        if (currentPatient.getCheckIns() == null || currentPatient.getCheckIns().size() == 0) {
            patientDao.assignEmptyForeignCollection(currentPatient, "checkIns");
        }

        loadPatientQuestions(currentPatient);

        if (patient.getQuestions() != null && patient.getQuestions().size() != 0) {
            syncQuestions(currentPatient, patient.getQuestions());
        }


        if (patient.getCheckIns() != null) {
            for (CheckInImpl checkIn : patient.getCheckIns()) {
                //TODO: update checkIns
            }
        }

        return currentPatient;
    }

    public void loadPatientQuestions(PatientImpl patient) throws SQLException {
        if (patient.questionIds != null && patient.questionIds.size() > 0) {
            Dao<QuestionImpl, Long> qDao = getDao(QuestionImpl.class);
            Collection<Long> ids = new ArrayList<Long>(patient.questionIds);
            for (Long id : ids) {
                QuestionImpl question = qDao.queryForId(id);
                if (question != null && !patient.getQuestions().contains(question)) {
                    patient.addQuestion(question);
                }
            }
        }
    }

    public void ensureDoctorForPatient(PatientImpl currentPatient, DoctorImpl doctor) throws SQLException {
        DoctorImpl currentDoctor = getStoredDoctorFromResponse(doctor);
        if (currentPatient.doctor == null) {

            currentPatient.doctor = currentDoctor;
            patientDaoHelper.getDao(PatientImpl.class).update(currentPatient);
            doctorDaoHelper.getDao(DoctorImpl.class).refresh(currentDoctor);
        }
    }

    public Dao getDao(Class clazz) {
        try {
            Dao<Object, Long> dao = symptomManagementHelper.getDao(clazz);
            return dao;
        } catch (SQLException ex) {
            ex.printStackTrace();
            return null;
        }
    }
}
