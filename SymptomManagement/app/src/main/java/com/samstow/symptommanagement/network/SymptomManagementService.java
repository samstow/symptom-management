package com.samstow.symptommanagement.network;

import android.content.Context;
import android.util.Log;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.samstow.symptommanagement.client.SecuredRestBuilder;
import com.samstow.symptommanagement.client.UserSvcApi;
import com.samstow.symptommanagement.event.FailureEvent;
import com.samstow.symptommanagement.event.bus.BusProvider;
import com.samstow.symptommanagement.model.CheckIn;
import com.samstow.symptommanagement.model.Doctor;
import com.samstow.symptommanagement.model.Patient;
import com.samstow.symptommanagement.model.Question;

import org.androidannotations.annotations.EBean;
import org.androidannotations.annotations.UiThread;
import org.apache.http.client.HttpClient;

import retrofit.ErrorHandler;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.ApacheClient;
import retrofit.client.Client;

/**
 * Created by samstow on 11/08/2014.
 */
@EBean(scope = EBean.Scope.Singleton)
public class SymptomManagementService {

    private static final String TAG = SymptomManagementService.class.getSimpleName();

    private static final String DEFAULT_CLIENT_ID = "mobile";

    private UserSvcApi serviceClient;
    private Context context;

    private static SymptomManagementService staticInstance;


    public static SymptomManagementService getInstance() {
        return staticInstance;
    }


    public SymptomManagementService(Context context) {
        Log.d(TAG, "Registering client");
        BusProvider.getInstance().register(this);
        this.context = context;

        if (staticInstance == null) {
            staticInstance = this;
        }
    }

    private static ObjectMapper getObjectMapper() {
        ObjectMapper mapper = new ObjectMapper();
        mapper.addMixInAnnotations(Patient.class, JacksonTypeMixin.PatientMixin.class);
        mapper.addMixInAnnotations(Doctor.class, JacksonTypeMixin.DoctorMixin.class);
        mapper.addMixInAnnotations(Question.class, JacksonTypeMixin.QuestionMixIn.class);
        mapper.addMixInAnnotations(com.samstow.symptommanagement.model.Response.class, JacksonTypeMixin.ResponseMixIn.class);
        mapper.addMixInAnnotations(CheckIn.class, JacksonTypeMixin.CheckInMixIn.class);
        mapper.setSerializationInclusion(JsonInclude.Include.NON_NULL);
        return mapper;
    }

    public UserSvcApi makeClient(String user, String password) {
        Client client = new ApacheClient((HttpClient)UnsafeHttpsClient.createUnsafeClient());
        RestAdapter restAdapter = SecuredRestBuilder
                .getBaseBuilder(SecuredRestBuilder.TEST_DEVICE_URL, getObjectMapper(), client)
                .setErrorHandler(getErrorHandler())
                .setUsername(user)
                .setPassword(password)
                .setClientId(DEFAULT_CLIENT_ID)
                .build();

        serviceClient = restAdapter.create(UserSvcApi.class);
        return serviceClient;
    }

    public UserSvcApi getServiceClient() {
        return serviceClient;
    }


    private ErrorHandler getErrorHandler() {
        return new ErrorHandler() {
            @Override
            public Throwable handleError(RetrofitError cause) {
                produceFailureEvent(cause);
                return cause;
            }
        };
    }

    @UiThread
    public void produceFailureEvent(final RetrofitError retrofitError) {
         BusProvider.getInstance().post(new FailureEvent(retrofitError));
    }




}
