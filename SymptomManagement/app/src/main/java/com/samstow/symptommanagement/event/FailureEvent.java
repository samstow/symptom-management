package com.samstow.symptommanagement.event;


import retrofit.RetrofitError;

/**
 * Created by samstow on 11/08/2014.
 */
public class FailureEvent {

    private RetrofitError retrofitError;

    public FailureEvent(RetrofitError retrofitError) {
        this.retrofitError = retrofitError;
    }

    public RetrofitError getRetrofitError() {
        return retrofitError;
    }

}
