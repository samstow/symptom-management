package com.samstow.symptommanagement.model;

import com.j256.ormlite.field.DatabaseField;

import java.util.ArrayList;

/**
 * Created by samstow on 9/11/14.
 */
public class MedicationQuestonFactory {

    private static final String QUESTION_PREFIX = "Did you take your ";
    private static final String MEDICATION = "pain medication";

    public static QuestionImpl createGenericQuestion() {
        return createMedicationQuestion(MEDICATION);
    }

    public static boolean isGenericMedQuestion(QuestionImpl question) {
        return MEDICATION.equals(question.getQuestionInfo());
    }

    public static QuestionImpl createMedicationQuestion(String medicationName) {
        return createMedicationQuestion(medicationName, 0);
    }

    public static QuestionImpl createMedicationQuestion(String medicationName, int priority) {
        String questionText = String.format("%s%s?", QUESTION_PREFIX, medicationName);
        QuestionImpl question = new QuestionImpl(
                questionText,
                ResponseType.DATED_BOOLEAN,
                QuestionType.MEDICATION,
                medicationName,
                priority);
        return question;
    }

    public void setMedicationName(String medicationName, QuestionImpl question) {
        question.setQuestionText(String.format("%s%s?", QUESTION_PREFIX, medicationName));
    }


}
