package com.samstow.symptommanagement.reminders;

/**
 * Created by samstow on 25/11/14.
 */
public class Constants {

    public static final String EXTRA_PATIENT = "Patient";
    public static final String EXTRA_REMINDER = "Reminder";

}
