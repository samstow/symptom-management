package com.samstow.symptommanagement.event;

import com.samstow.symptommanagement.model.Patient;
import com.samstow.symptommanagement.model.PatientImpl;

/**
 * Created by samstow on 30/11/14.
 */
public class PatientSelectedEvent {

    public PatientImpl patient;

    public PatientSelectedEvent(PatientImpl patient) {
        this.patient = patient;
    }
}
