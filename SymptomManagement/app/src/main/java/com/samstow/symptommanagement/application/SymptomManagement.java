package com.samstow.symptommanagement.application;

import android.app.Application;
import android.content.Intent;

import com.samstow.symptommanagement.reminders.AlarmReceiver_;
import com.samstow.symptommanagement.reminders.Constants;

import net.danlew.android.joda.JodaTimeAndroid;

/**
 * Created by samstow on 29/11/14.
 */
public class SymptomManagement extends Application {
    @Override
    public void onCreate() {
        super.onCreate();
        JodaTimeAndroid.init(this);
    }
}
