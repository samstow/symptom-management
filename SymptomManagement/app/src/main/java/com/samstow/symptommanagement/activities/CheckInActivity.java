package com.samstow.symptommanagement.activities;

import android.app.Activity;
import android.app.Fragment;
import android.app.FragmentTransaction;
import android.os.Bundle;
import android.widget.Toast;

import com.samstow.symptommanagement.R;
import com.samstow.symptommanagement.database.PersistenceManager;
import com.samstow.symptommanagement.event.QuestionResponseEvent;
import com.samstow.symptommanagement.event.bus.BusProvider;
import com.samstow.symptommanagement.fragments.DatedBooleanQuestionFragment;
import com.samstow.symptommanagement.fragments.DatedBooleanQuestionFragment_;
import com.samstow.symptommanagement.fragments.QuestionFragment;
import com.samstow.symptommanagement.fragments.RadioQuestionFragment_;
import com.samstow.symptommanagement.model.CheckIn;
import com.samstow.symptommanagement.model.CheckInImpl;
import com.samstow.symptommanagement.model.PatientImpl;
import com.samstow.symptommanagement.model.QuestionImpl;
import com.samstow.symptommanagement.model.ResponseImpl;
import com.samstow.symptommanagement.network.SymptomManagementService;
import com.squareup.otto.Subscribe;

import org.androidannotations.annotations.AfterExtras;
import org.androidannotations.annotations.Background;
import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.Extra;
import org.androidannotations.annotations.UiThread;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import retrofit.RetrofitError;

@EActivity(R.layout.activity_check_in)
public class CheckInActivity extends Activity {


    @Extra
    public CheckInImpl checkIn;
    @Extra
    public PatientImpl patient;

    @Bean
    public PersistenceManager persistenceManager;

    @Bean
    public SymptomManagementService service;

    private List<QuestionImpl> questions;
    private int currentQuestionIndex;

    @Override
    protected void onResume() {
        super.onResume();
        BusProvider.getInstance().register(eventHandler);
    }

    @Override
    protected void onPause() {
        super.onPause();
        BusProvider.getInstance().unregister(eventHandler);
    }

    @Override
    protected void onStop() {
        super.onStop();
        //TODO: check checkin status, if not completed, delete.

    }

    private Object eventHandler = new Object() {

        @Subscribe
        public void onResponseEvent(QuestionResponseEvent responseEvent) {
            //Add response for question to checkin
            persistenceManager.handleResponseForCheckIn(responseEvent.getResponse(), checkIn);
            currentQuestionIndex = responseEvent.getNextQuestionIndex();
            //Show next question
            showNextQuestion();
        }
    };

    private void showNextQuestion() {
        if (currentQuestionIndex < questions.size()) {
            QuestionImpl question = questions.get(currentQuestionIndex);
            showQuestionFragment(question);
        } else {
            completeCheckIn();
        }
    }

    @Background
    public void completeCheckIn() {
        checkIn.setComplete(true);
        CheckInImpl submittedCheckIn = null;
        try {
           submittedCheckIn = (CheckInImpl)service.getServiceClient().checkIn(checkIn);
           checkIn.setSubmittedDate(submittedCheckIn.getSubmittedDate());
            persistenceManager.saveCheckIn(checkIn);
            showCheckInSummary();
        } catch (RetrofitError e) {

        }

    }

    @UiThread
    public void showCheckInSummary() {
        Fragment fragment = getFragmentManager().findFragmentById(R.id.container);
        getFragmentManager().beginTransaction()
                .remove(fragment)
                .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_CLOSE)
                .commit();
        Toast.makeText(this, "Check In complete", Toast.LENGTH_SHORT).show();
        finish();
    }



    //TODO show summary of questions and ability to send completed check in

    @AfterExtras
    public void getPatientQuestionsAndStartCheckIn() {

        try {
            patient = persistenceManager.getStoredPatientFromResponse(patient);
        } catch (SQLException ex) {
            ex.printStackTrace();
        }

        service.makeClient(patient.getUsername(), patient.getPassword());
        if (checkIn == null) {
            checkIn = persistenceManager.createCheckIn(patient);
        }
        questions = patient.getQuestionsByPriority();
        if (questions.size() > 0) {
            QuestionImpl question = (QuestionImpl)questions.get(0);
            currentQuestionIndex = 0;
            showQuestionFragment(question);
        }
    }

    private void showQuestionFragment(QuestionImpl question) {
        QuestionFragment fragment = null;

        //TODO: implement fragments for different response types
        switch (question.getResponseType()) {
            case FREE_TEXT:
                break;
            case RADIO_OPTION:
                fragment = RadioQuestionFragment_.builder().question(question).patient(patient).build();
                break;
            case CHECKED_OPTIONS:
                break;
            case BOOLEAN:
                break;
            case DATED_BOOLEAN:
                fragment = DatedBooleanQuestionFragment_.builder().question(question).patient(patient).build();
                break;
        }
        if (fragment == null) {
            currentQuestionIndex++;
            showNextQuestion();
        }

        getFragmentManager().beginTransaction()
                .replace(R.id.container, fragment)
                .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN)
                .commit();
    }

}
