package com.samstow.symptommanagement.reminders;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;

import com.j256.ormlite.dao.Dao;
import com.samstow.symptommanagement.R;
import com.samstow.symptommanagement.activities.CheckInActivity_;
import com.samstow.symptommanagement.database.SymptomManagementHelper;
import com.samstow.symptommanagement.model.Patient;
import com.samstow.symptommanagement.model.PatientImpl;
import com.samstow.symptommanagement.model.Reminder;

import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.EReceiver;
import org.androidannotations.annotations.Extra;
import org.androidannotations.annotations.OrmLiteDao;
import org.androidannotations.annotations.ReceiverAction;
import org.androidannotations.annotations.SystemService;
import org.androidannotations.annotations.res.StringRes;

@EReceiver
public class  AlarmReceiver extends BroadcastReceiver {
	
	private static final String TAG = "AlarmReceiver";
    @SystemService
    NotificationManager notificationManager;

    @StringRes(R.string.app_name)
    String symptomManagement;

    @StringRes(R.string.notification_text)
    String notificationText;

    @OrmLiteDao(helper = SymptomManagementHelper.class, model = Reminder.class)
    Dao<Reminder, Long> reminderDao;

    @ReceiverAction
    void onReceiveAction(@ReceiverAction.Extra(value = Constants.EXTRA_PATIENT) PatientImpl patient, @ReceiverAction.Extra(value = Constants.EXTRA_REMINDER) Reminder reminder, Context context, Intent intent) {

        Intent intent1 = AlarmService.makeIntent(patient, reminder);
        AlarmService_.intent(context).scheduleNext(intent1).start();

        Intent checkInIntent = new CheckInActivity_.IntentBuilder_(context).patient(patient).get();
        PendingIntent pendingIntent = PendingIntent.getActivity(context, 0, checkInIntent, 0);
		Notification n = new Notification.Builder(context)
                                        .setSmallIcon(R.drawable.ic_launcher)
                                        .setTicker(notificationText)
                                        .setWhen(System.currentTimeMillis())
                                        .setContentIntent(pendingIntent)
                                        .setContentTitle(symptomManagement)
                                        .setContentText(notificationText)
                                        .build();

//		if (shouldVibrate) {
			n.defaults |= Notification.DEFAULT_VIBRATE;
//		}
//		if (shouldAleraSound) {
			n.sound = Uri.parse(android.provider.Settings.System.DEFAULT_NOTIFICATION_URI.toString());
			n.defaults |= Notification.DEFAULT_SOUND;
//		}
		n.flags |= Notification.FLAG_AUTO_CANCEL;		
		

		notificationManager.notify((int)reminder.getId(), n);
	}

    @Override
    public void onReceive(Context context, Intent intent) {

    }
}
