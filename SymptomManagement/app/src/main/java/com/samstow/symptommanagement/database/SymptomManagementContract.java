package com.samstow.symptommanagement.database;

import android.content.ContentResolver;
import android.net.Uri;
import android.provider.BaseColumns;

/**
 * Created by samstow on 14/08/2014.
 */
public class SymptomManagementContract {

    public static final String AUTHORITY = "com.samstow.symptommanagement";
    private SymptomManagementContract() {

    }

    public static final class PatientContract implements BaseColumns {

        public static final String TABLE_NAME = "Patient";

        public static final String CONTENT_URI_PATH = "patients";

        public static final String MIMETYPE_TYPE = "patients";
        public static final String MIMETYPE_NAME = "com.samstow.symptommanagement.provider";

        public static final int CONTENT_URI_PATTERN_MANY = 1;
        public static final int CONTENT_URI_PATTERN_ONE = 2;

        public static final Uri CONTENT_URI = new Uri.Builder().scheme(ContentResolver.SCHEME_CONTENT).authority(AUTHORITY).appendPath(CONTENT_URI_PATH).build();

//        public static final String ASSET_ID = "AssetId";
//        public static final String ASSET_NAME = "AssetName";
//        public static final String ASSET_TREE_TYPE = "AssetTreeType";
//        public static final String PARENT_ASSET_ID = "ParentAssetId";
    }
}
