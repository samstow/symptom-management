package com.samstow.symptommanagement.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.JsonSerializable;
import com.j256.ormlite.field.DataType;
import com.j256.ormlite.field.DatabaseField;

import java.io.Serializable;
import java.text.DateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by samstow on 8/11/14.
 */
public class ResponseImpl implements Response, Serializable {

    //Native DB Id
    @JsonIgnore
    @DatabaseField(generatedId = true)
    private long id;
    //Server Id
    @JsonProperty("id")
    @DatabaseField
    protected String serverId;
    @JsonIgnore
    @DatabaseField(foreign = true)
    CheckInImpl checkIn;
    @DatabaseField
    private String question;
    @DatabaseField
    private String responseText;
    @DatabaseField
    private boolean booleanResponse;
    @DatabaseField
    private Date yesTime;
    @DatabaseField
    private ResponseType responseType;
    @DatabaseField(dataType = DataType.SERIALIZABLE)
    private ArrayList<String> responses;

    static public ResponseImpl createRadioResponse(String question, String responseText) {
        ResponseImpl response = new ResponseImpl(question);
        response.responseType = ResponseType.RADIO_OPTION;
        response.responseText = responseText;
        return response;
    }

    static public ResponseImpl createDatedBooleanResponse(String question, boolean response, Date yesTime) {
        ResponseImpl responseImpl = new ResponseImpl(question);
        responseImpl.responseType = ResponseType.DATED_BOOLEAN;
        responseImpl.booleanResponse = response;
        if (response) {
            responseImpl.yesTime = yesTime;
        }
        return responseImpl;
    }

    public ResponseImpl() {
        this.id = -1;
    }

    public ResponseImpl(String question) {
        this.id = -1;
        this.question = question;
    }

    public String getTextRepresentation() {
        String text = "";
        switch (this.responseType) {
            case RADIO_OPTION:
                text = responseText;
                break;
            case DATED_BOOLEAN:
                text = getResponseString();
                break;
            default:
                break;
        }
        return text;
    }

    private String getResponseString() {
        if (!booleanResponse) {
            return "NO";
        } else if (yesTime != null) {
            DateFormat dateFormat = DateFormat.getTimeInstance(DateFormat.MEDIUM);
            String time = dateFormat.format(yesTime);
            return String.format("YES (%s)", time);
        } else {
            return "YES";
        }
    }

    public long getAndroid_ID() {
        return id;
    }

    @Override
    public String getId() {
        return serverId;
    }

    @Override
    public String getQuestion() {
        return question;
    }

    @Override
    public String getResponseText() {
        return responseText;
    }

    @Override
    public boolean getResponse() {
        return booleanResponse;
    }

    @Override
    public Date getYesTime() {
        return yesTime;
    }

    @Override
    public ResponseType getResponseType() {
        return responseType;
    }

    @Override
    public List<String> getResponses() {
        return responses;
    }

    public void setCheckIn(CheckInImpl checkIn) {
        this.checkIn = checkIn;
    }

    public void setQuestion(String question) {
        this.question = question;
    }

    public void setResponseText(String responseText) {
        this.responseText = responseText;
    }

    public void setResponse(boolean booleanResponse) {
        this.booleanResponse = booleanResponse;
    }

    public void setYesTime(Date yesTime) {
        this.yesTime = yesTime;
    }
}
