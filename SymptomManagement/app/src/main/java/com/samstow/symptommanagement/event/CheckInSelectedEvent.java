package com.samstow.symptommanagement.event;

import com.samstow.symptommanagement.model.CheckInImpl;

/**
 * Created by samstow on 30/11/14.
 */
public class CheckInSelectedEvent {

    public CheckInImpl checkIn;

    public CheckInSelectedEvent(CheckInImpl checkIn) {
        this.checkIn = checkIn;
    }
}
