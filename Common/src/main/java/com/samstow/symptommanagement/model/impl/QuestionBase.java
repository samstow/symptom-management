package com.samstow.symptommanagement.model.impl;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.samstow.symptommanagement.model.Question;
import com.samstow.symptommanagement.model.QuestionType;
import com.samstow.symptommanagement.model.Response;
import com.samstow.symptommanagement.model.ResponseType;

import java.io.Serializable;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by samstow on 5/11/14.
 */
public class QuestionBase implements Question, Serializable {

    protected String serverId;
    private String questionText;
    private ArrayList<String> responseOptions = new ArrayList<String>();
    private int priority = 0;
    private ResponseType responseType;
    private QuestionType questionType;
    private String questionInfo;

    @Override
    public String getServerId() {
        return serverId;
    }

    @Override
    public String getQuestionText() {
        return questionText;
    }

    @Override
    public List<String> getResponseOptions() {
        return responseOptions;
    }

    @Override
    public int getPriority() {
        return priority;
    }

    @Override
    public ResponseType getResponseType() {
        return responseType;
    }

    @Override
    public QuestionType getQuestionType() {
        return questionType;
    }

    @Override
    public String getQuestionInfo() {
        return questionInfo;
    }

    @Override
    public boolean equals(Object o) {
        if (o instanceof Question) {
            Question q = (Question)o;
            return q.getQuestionText().equals(questionText);
        }
        return super.equals(o);
    }
}
