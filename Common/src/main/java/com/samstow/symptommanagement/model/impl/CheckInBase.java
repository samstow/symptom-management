package com.samstow.symptommanagement.model.impl;

import com.samstow.symptommanagement.model.CheckIn;
import com.samstow.symptommanagement.model.Response;

import java.util.Collection;
import java.util.Date;

/**
 * Created by samstow on 30/11/14.
 */
public class CheckInBase implements CheckIn {

    protected String id;
    private Collection<Response> responses;
    private Date time;
    private Date submittedDate;

    @Override
    public String getId() {
        return id;
    }

    @Override
    public Collection<? extends Response> getResponses() {
        return responses;
    }

    @Override
    public Date getTime() {
        return time;
    }

    @Override
    public Date getSubmittedDate() {
        return submittedDate;
    }
}
