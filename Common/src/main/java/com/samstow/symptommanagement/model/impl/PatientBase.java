package com.samstow.symptommanagement.model.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.samstow.symptommanagement.model.CheckIn;
import com.samstow.symptommanagement.model.Patient;
import com.samstow.symptommanagement.model.Question;

@JsonIgnoreProperties(ignoreUnknown = true)
public class PatientBase extends UserBase implements Patient {

	protected String patientRecordNumber;
	protected Date dateOfBirth;
	protected String doctorRegistrationId;
	protected List<Question> questions;
	protected List<CheckIn> checkIns;
	
	public PatientBase() {
		super();
		questions = new ArrayList<Question>();
		checkIns = new ArrayList<CheckIn>();
	}
	
	public String getPatientRecordNumber() {
		return patientRecordNumber;
	}
	public void setPatientRecordNumber(String patientRecordNumber) {
		this.patientRecordNumber = patientRecordNumber;
	}

    public Date getDateOfBirth() {
		return dateOfBirth;
	}
	public void setDateOfBirth(Date dateOfBirth) {
		this.dateOfBirth = dateOfBirth;
	}
	public String getDoctorRegistrationId() {
		return doctorRegistrationId;
	}
	public void setDoctorRegistrationId(String doctorRegistrationId) {
		this.doctorRegistrationId = doctorRegistrationId;
	}
	public List<Question> getQuestions() {
		return questions;
	}
	public void setQuestions(List<Question> questions) {
		this.questions = questions;
	}
	public List<CheckIn> getCheckIns() {
		return checkIns;
	}
	public void setCheckInIds(List<CheckIn> checkIns) {
		this.checkIns = checkIns;
	}
	
    public List<Question> getQuestionsByPriority() {
        //TODO
        return questions;
    }

}
