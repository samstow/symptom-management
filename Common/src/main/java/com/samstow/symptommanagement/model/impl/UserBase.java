package com.samstow.symptommanagement.model.impl;

import java.util.ArrayList;
import java.util.List;

import com.samstow.symptommanagement.model.User;

public class UserBase implements User {

	protected String serverId;
	protected String username;
	protected String password;
	protected String firstName;
	protected String lastName;
	protected String phone;
	protected List<String> roles;
    	
    public UserBase() {
    	this.roles = new ArrayList<String>();
    }
    
    
	public String getServerId() {
		return serverId;
	}

	public void setServerId(String id) {
		this.serverId = id;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	@Override
	public List<String> getRoles() {
		return roles;
	}

}
