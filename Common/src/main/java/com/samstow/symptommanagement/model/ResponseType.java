package com.samstow.symptommanagement.model;

/**
 * Created by samstow on 5/11/14.
 */
public enum ResponseType {
    FREE_TEXT,
    RADIO_OPTION,
    CHECKED_OPTIONS,
    BOOLEAN,
    DATED_BOOLEAN
}
