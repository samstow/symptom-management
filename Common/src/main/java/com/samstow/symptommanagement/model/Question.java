package com.samstow.symptommanagement.model;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.samstow.symptommanagement.model.impl.QuestionBase;

import java.util.List;

/**
 * Created by samstow on 19/10/2014.
 */
@JsonDeserialize(as= QuestionBase.class)
public interface Question {
    public String getServerId();
    public String getQuestionText();
    public List<String> getResponseOptions();
    //Lowest number means highest priority, means answered first
    public int getPriority();
    public ResponseType getResponseType();
    public QuestionType getQuestionType();
    //If symptom Question, refers to symptom name, if medication question, refers to medication name
    public String getQuestionInfo();

}
