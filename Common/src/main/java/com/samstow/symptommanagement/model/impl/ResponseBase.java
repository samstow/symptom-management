package com.samstow.symptommanagement.model.impl;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.samstow.symptommanagement.model.Response;
import com.samstow.symptommanagement.model.ResponseType;

import java.util.ArrayList;
import java.util.Date;

/**
 * Created by samstow on 30/11/14.
 */
public class ResponseBase implements Response {

    protected String id;
    private String question;
    private String responseText;
    private boolean response;
    private Date yesTime;
    private ResponseType responseType;
    private ArrayList<String> responses;

    @Override
    public String getId() {
        return id;
    }

    @Override
    public String getQuestion() {
        return question;
    }

    @Override
    public String getResponseText() {
        return responseText;
    }

    @Override
    public boolean getResponse() {
        return response;
    }

    @Override
    public Date getYesTime() {
        return yesTime;
    }

    @Override
    public ResponseType getResponseType() {
        return responseType;
    }

    @Override
    public ArrayList<String> getResponses() {
        return responses;
    }
}
