package com.samstow.symptommanagement.model;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.samstow.symptommanagement.model.impl.ResponseBase;

import java.util.Date;
import java.util.List;

/**
 * Created by samstow on 19/10/2014.
 */
@JsonDeserialize(as= ResponseBase.class)
public interface Response {
    public String getId();
    public String getQuestion();
    public ResponseType getResponseType();
    public String getResponseText();
    public List<String> getResponses();
    public Date getYesTime();
    public boolean getResponse();

}
