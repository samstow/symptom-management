package com.samstow.symptommanagement.model;

import java.util.List;



/**
 * Created by samstow on 1/11/14.
 */
public interface User {

    //requires
    //id
    //firstName
    //lastName
    //phone
    //username (email)
    //authorities
	
    public String getFirstName();

	public void setFirstName(String firstName);

	public String getLastName();

	public void setLastName(String lastName);

	public String getPhone();

	public void setPhone(String phone);

    public String getServerId();
    
    public String getPassword();
    
    public void setPassword(String password);
    
	public String getUsername();
	
	public List<String> getRoles();

}
