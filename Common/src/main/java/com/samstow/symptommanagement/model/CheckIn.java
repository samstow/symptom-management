package com.samstow.symptommanagement.model;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.samstow.symptommanagement.model.impl.CheckInBase;

import java.util.Collection;
import java.util.Date;
import java.util.List;

/**
 * Created by samstow on 19/10/2014.
 */
@JsonDeserialize(as= CheckInBase.class)
public interface CheckIn {
    public String getId();
    public Date getTime();
    public Collection<? extends Response> getResponses();
    public Date getSubmittedDate();
}
