package com.samstow.symptommanagement.model;

import java.util.Collection;
import java.util.List;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.samstow.symptommanagement.model.impl.DoctorBase;

/**
 * Created by samstow on 19/10/2014.
 */
//@JsonDeserialize(as=DoctorBase.class)
public interface Doctor extends User {

    //requires
    //doctorRegistrationNumber
    //patientRecordNumbers
	public String getDoctorRegistrationId();
	public Collection<? extends Patient> getPatients();
	
}
