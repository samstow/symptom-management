package com.samstow.symptommanagement.model;

/**
 * Created by samstow on 30/11/14.
 */
public enum QuestionType {
    SYMPTOM,
    MEDICATION,
    GENERAL
}
