package com.samstow.symptommanagement.model;

import java.util.Collection;
import java.util.Date;
import java.util.List;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.samstow.symptommanagement.model.impl.PatientBase;

/**
 * Created by samstow on 19/10/2014.
 */
//@JsonDeserialize(as=PatientBase.class)
public interface Patient extends User {

    //requires
    //recordnumber
    //dob
    //questionIds
    //doctorId
    //checkInIds
    //reminderIds??
	
	public String getPatientRecordNumber();
	public Date getDateOfBirth();
	public void setPatientRecordNumber(String recordNumber);
	public void setDateOfBirth(Date dateOfBirth);
	
	public Collection<? extends Question> getQuestions();
	public String getDoctorRegistrationId();
	public Collection<? extends CheckIn> getCheckIns();
}
