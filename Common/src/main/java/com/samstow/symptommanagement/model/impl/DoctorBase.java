package com.samstow.symptommanagement.model.impl;

import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.samstow.symptommanagement.model.Doctor;
import com.samstow.symptommanagement.model.Patient;

@JsonIgnoreProperties(ignoreUnknown = true)
public class DoctorBase extends UserBase implements Doctor {

    private List<Patient> patients;

	public DoctorBase() {
		super();
		this.patients = new ArrayList<Patient>();
	}

	@Override
	public String getDoctorRegistrationId() {
		return getServerId();
	}


    @Override
    public List<Patient> getPatients() {
        return patients;
    }
    
    public void setPatients(List<Patient> patients) {
    	this.patients = patients;
    }

}
