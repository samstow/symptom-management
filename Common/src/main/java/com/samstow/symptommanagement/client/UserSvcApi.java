package com.samstow.symptommanagement.client;

import java.util.Collection;
import java.util.List;

import retrofit.http.Body;
import retrofit.http.GET;
import retrofit.http.POST;
import retrofit.http.PUT;
import retrofit.http.Path;
import retrofit.http.Query;

import com.samstow.symptommanagement.model.CheckIn;
import com.samstow.symptommanagement.model.Doctor;
import com.samstow.symptommanagement.model.Patient;

/**
 * This interface defines an API for a VideoSvc. The
 * interface is used to provide a contract for client/server
 * interactions. The interface is annotated with Retrofit
 * annotations so that clients can automatically convert the
 * 
 * 
 * @author jules
 *
 */
public interface UserSvcApi {
	
	public static final String ROLE_DOCTOR = "ROLE_DOCTOR";
	public static final String ROLE_PATIENT = "ROLE_PATIENT";
	
	public static final String PASSWORD_PARAMETER = "password";

	public static final String USERNAME_PARAMETER = "username";
	
	public static final String TOKEN_PATH = "/oauth/token";
	
	// The path where we expect the DoctorSvc to live
	public static final String DOCTOR_SVC_PATH = "/doctor";
	public static final String PATIENT_SVC_PATH = "/patient";
	
	public static final String SEARCH_NAME_PATH = "/search/findByUsername";
	public static final String CHECK_IN_PATH = "/checkIn";
	public static final String BY_ID_PATH = "/{id}";
	public static final String ME_PATH = "/me";
	public static final String MINE_PATH = "/mine";
	
	@GET(ME_PATH)
	public List<String> getUserRoles();
	
	//Must be doctor
	@POST(PATIENT_SVC_PATH)
	public Patient addPatient(@Body Patient patient);
	
	//Must be doctor
	@GET(DOCTOR_SVC_PATH + ME_PATH)
	public Doctor getMyDetailsDoctor();
	
	//Must be patient
	@GET(PATIENT_SVC_PATH + ME_PATH)
	public Patient getMyDetailsPatient();
	
	//Must be doctor
	@PUT(DOCTOR_SVC_PATH + ME_PATH)
	public Doctor updateMyDetails(@Body Doctor doctor);
	
	//Must be patient
	@PUT(PATIENT_SVC_PATH + ME_PATH)
	public Patient updateMyDetails(@Body Patient patient);
	
	//Must be doctor
	@GET(PATIENT_SVC_PATH)
	public Collection<? extends Patient> getPatients();
	
	//Must be doctor
	@GET(PATIENT_SVC_PATH + BY_ID_PATH)
	public Patient getPatient(@Path("id") String id);
	
	//Must be patient
	@GET(DOCTOR_SVC_PATH + MINE_PATH)
	public Doctor getDoctor();
	
	//Must be doctor
	@PUT(PATIENT_SVC_PATH)
	public Patient updatePatient(@Body Patient patient);
	
	//Must be doctor
	@GET(PATIENT_SVC_PATH + SEARCH_NAME_PATH)
	public Collection<? extends Patient> findPatientsByUsername(@Query(USERNAME_PARAMETER) String username);
	
	//Must be patient
	@POST(PATIENT_SVC_PATH + ME_PATH + CHECK_IN_PATH)
	public CheckIn checkIn(@Body CheckIn checkIn);
	
	//Must be patient
	@GET(PATIENT_SVC_PATH + ME_PATH + CHECK_IN_PATH)
	public Collection<? extends CheckIn> getMyCheckIns();
	
	//Must be doctor
	@GET(PATIENT_SVC_PATH + BY_ID_PATH + CHECK_IN_PATH)
	public Collection<? extends CheckIn> getPatientCheckIns(@Path("id") String id);

}
