package com.samstow.symptommanagement.integration.test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.fail;

import java.util.Calendar;
import java.util.Collection;
import java.util.List;
import java.util.UUID;

import org.junit.Test;

import retrofit.RetrofitError;

import com.fasterxml.jackson.databind.JsonNode;
import com.samstow.symptommanagement.client.SecuredRestBuilder;
import com.samstow.symptommanagement.client.SecuredRestException;
import com.samstow.symptommanagement.client.UserSvcApi;
import com.samstow.symptommanagement.model.Doctor;
import com.samstow.symptommanagement.model.Patient;
import com.samstow.symptommanagement.model.impl.PatientBase;
import com.samstow.symptommanagement.model.impl.PatientImpl;
import com.samstow.symptommanagement.model.impl.QuestionImpl;


/**
 * 
 * This integration test sends a POST request to the VideoServlet to add a new
 * video and then sends a second GET request to check that the video showed up
 * in the list of videos. Actual network communication using HTTP is performed
 * with this test.
 * 
 * The test requires that the VideoSvc be running first (see the directions in
 * the README.md file for how to launch the Application).
 * 
 * To run this test, right-click on it in Eclipse and select
 * "Run As"->"JUnit Test"
 * 
 * Pay attention to how this test that actually uses HTTP and the test that just
 * directly makes method calls on a VideoSvc object are essentially identical.
 * All that changes is the setup of the videoService variable. Yes, this could
 * be refactored to eliminate code duplication...but the goal was to show how
 * much Retrofit simplifies interaction with our service!
 * 
 * @author jules
 *
 */
public class SymptomManagementSvcClientApiTest {

	private final String USERNAME = "sam";
	private final String DOCTOR = "doctor";
	private final String PATIENT = "patient";
	
	private final String PASSWORD = "pass";
	private final String CLIENT_ID = "mobile";
	private final String READ_ONLY_CLIENT_ID = "mobileReader";

	
	@SuppressWarnings("unused")
	private UserSvcApi symptomService = 
			SecuredRestBuilder.getBaseBuilder(SecuredRestBuilder.TEST_URL, SecuredRestBuilder.getObjectMapper())
			.setUsername(USERNAME)
			.setPassword(PASSWORD)
			.setClientId(CLIENT_ID)
			.build()
			.create(UserSvcApi.class);
	
	private UserSvcApi symptomDoctorService = 
			SecuredRestBuilder.getBaseBuilder(SecuredRestBuilder.TEST_URL, SecuredRestBuilder.getObjectMapper())
	.setUsername(DOCTOR)
	.setPassword(PASSWORD)
	.setClientId(CLIENT_ID)
	.build()
	.create(UserSvcApi.class);
	
	private UserSvcApi symptomPatientService = 
			SecuredRestBuilder.getBaseBuilder(SecuredRestBuilder.TEST_URL, SecuredRestBuilder.getObjectMapper())
	.setUsername(PATIENT)
	.setPassword(PASSWORD)
	.setClientId(CLIENT_ID)
	.build()
	.create(UserSvcApi.class);

	private UserSvcApi readOnlySymptomService = 
			SecuredRestBuilder.getBaseBuilder(SecuredRestBuilder.TEST_URL, SecuredRestBuilder.getObjectMapper())
			.setUsername(DOCTOR)
			.setPassword(PASSWORD)
			.setClientId(READ_ONLY_CLIENT_ID)
			.build()
			.create(UserSvcApi.class);

	@SuppressWarnings("unused")
	private UserSvcApi invalidClientSymptomService = 
			SecuredRestBuilder.getBaseBuilder(SecuredRestBuilder.TEST_URL, SecuredRestBuilder.getObjectMapper())
			.setUsername(UUID.randomUUID().toString())
			.setPassword(UUID.randomUUID().toString())
			.setClientId(UUID.randomUUID().toString())
			.build()
			.create(UserSvcApi.class);

//	private Video video = TestData.randomVideo();

	/**
	 * This test creates a Video, adds the Video to the VideoSvc, and then
	 * checks that the Video is included in the list when getVideoList() is
	 * called.
	 * 
	 * @throws Exception
	 */
	@SuppressWarnings("unused")
	@Test
	public void testGetDoctorDetails() throws Exception {
		
		Doctor doctor = symptomDoctorService.getMyDetailsDoctor();
		Collection<? extends Patient> patients = symptomDoctorService.getPatients();
		Collection<? extends Patient> pats = symptomDoctorService.findPatientsByUsername("pat");

		try {
			String newUsername = "marcus";
			PatientImpl newPatient = new PatientImpl();
			newPatient.setUsername(newUsername);
			newPatient.setPassword("pass");
			newPatient.setFirstName("marcus");
			newPatient.setLastName("aurelius");
			newPatient.setDateOfBirth(Calendar.getInstance().getTime());
			
			Patient patientResult = symptomDoctorService.addPatient(newPatient);
		} catch (RetrofitError e) {
			assert (e.getCause() instanceof SecuredRestException);
		}
		
		doctor.setFirstName("sammy");
		symptomDoctorService.updateMyDetails(doctor);
		Doctor doctorRepeat = symptomDoctorService.getMyDetailsDoctor();
		assert(doctorRepeat.getFirstName().equals("sammy"));
		
		List<String> roles = symptomDoctorService.getUserRoles();
		assert(roles.contains(UserSvcApi.ROLE_DOCTOR));
		
		int i = 0;
	}
	


//	/**
//	 * This test ensures that clients with invalid credentials cannot get
//	 * access to videos.
//	 * 
//	 * @throws Exception
//	 */
	@SuppressWarnings("unused")
	@Test
	public void testAccessDeniedWithIncorrectCredentials() throws Exception {

		try {
			// Add the video
			symptomDoctorService.getMyDetailsPatient();

			fail("The server should have prevented the client from adding a video"
					+ " because it presented invalid client/user credentials");
		} catch (RetrofitError e) {
			assert (e.getCause() instanceof SecuredRestException);
		}
		Patient me = symptomPatientService.getMyDetailsPatient();
		me.setFirstName("sian");
		Patient me2 = symptomPatientService.updateMyDetails(me);
		
		int i = 1;
		String ii ="";
	}
//	
//	/**
//	 * This test ensures that read-only clients can access the video list
//	 * but not add new videos.
//	 * 
//	 * @throws Exception
//	 */
	@Test
	public void testReadOnlyClientAccess() throws Exception {

		Collection<? extends Patient> pattys = readOnlySymptomService.getPatients();
		assertNotNull(pattys);
		
		try {
			// Add the video
			readOnlySymptomService.addPatient(new PatientBase());

			fail("The server should have prevented the client from adding a video"
					+ " because it is using a read-only client ID");
		} catch (RetrofitError e) {
			JsonNode body = (JsonNode)e.getBodyAs(JsonNode.class);
			JsonNode error = body.get("error");
			assertEquals("insufficient_scope", error.asText()); 
		}
	}


}
