package com.samstow.symptommanagement.client;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

import com.samstow.symptommanagement.credentials.Credentials;
import com.samstow.symptommanagement.model.CheckIn;
import com.samstow.symptommanagement.model.Doctor;
import com.samstow.symptommanagement.model.Patient;
import com.samstow.symptommanagement.model.Question;
import com.samstow.symptommanagement.model.User;
import com.samstow.symptommanagement.model.impl.CheckInImpl;
import com.samstow.symptommanagement.model.impl.DoctorImpl;
import com.samstow.symptommanagement.model.impl.PatientImpl;
import com.samstow.symptommanagement.model.impl.QuestionImpl;
import com.samstow.symptommanagement.persistence.MongoUserDetailsService;

// Tell Spring that this class is a Controller that should 
// handle certain HTTP requests for the DispatcherServlet
@Controller
public class SymptomManagementSvc implements UserSvcApi {
	
	@Autowired
	MongoTemplate mongoTemplate;
	
	@Autowired
	MongoUserDetailsService userService;
	
	@Autowired
	PasswordEncoder encoder;
	
	private Credentials getCredentials() {
		Authentication authentication =
			      SecurityContextHolder.getContext().getAuthentication();
			    Credentials credentials = (Credentials) (authentication == null ? null : authentication.getPrincipal());
			    return credentials;
	}
	
	private DoctorImpl getCurrentDoctor() {
		Credentials current = getCredentials();
		if (!current.getRoles().contains(ROLE_DOCTOR)) {
			throw new SecuredRestException("Authenticated doctor not found");
		}
		DoctorImpl doctor = null;
		try {
			Criteria criteria = Criteria.where("id").is(current.getServerId());
			doctor = (DoctorImpl) mongoTemplate.findOne(Query.query(criteria), DoctorImpl.class);
		} catch (Exception e) {
			throw new SecuredRestException("Authenticated doctor not found");
		}
		if (doctor == null) {
			throw new SecuredRestException("Authenticated doctor not found");
		}
		return doctor;
	}
	
	private PatientImpl getCurrentPatient() {
		Credentials current = getCredentials();
		if (!current.getRoles().contains(ROLE_PATIENT)) {
			throw new SecuredRestException("Authenticated patient not found");
		}
		PatientImpl patient = null;
		try {
			Criteria criteria = Criteria.where("id").is(current.getServerId());
			patient = (PatientImpl) mongoTemplate.findOne(Query.query(criteria), PatientImpl.class);
		} catch (Exception e) {
			throw new SecuredRestException("Authenticated patient not found");
		}
		if (patient == null) {
			throw new SecuredRestException("Authenticated patient not found");
		}
		return patient;
	}
	
	@ExceptionHandler(SecuredRestException.class)
	@ResponseStatus(value = HttpStatus.FORBIDDEN)
	@ResponseBody
	public String handleSecuredRestException(SecuredRestException ex)
	{
	    return ex.getMessage();
	}
	
	private Credentials credentialsLessAuthorities(Credentials credentials) {
		credentials.setAuthorities(new ArrayList<>());
		return credentials;
	}
	
	private Collection<Credentials> safeListCredentials(Collection<? extends Credentials> credentials) {
		List<Credentials> patients = new ArrayList<Credentials>();
		for (Credentials patient: credentials) {
			patients.add(credentialsLessAuthorities(patient));
		}
		return patients;
	}
	
	private void safeEncodePassword(User credentials, String myPassword) {
		String password = credentials.getPassword();
		
		if (password != null) {
			if (!password.equals(myPassword)) {
				credentials.setPassword(encoder.encode(password));
			}
		}
	}
	

	@RequestMapping(value=PATIENT_SVC_PATH, method=RequestMethod.POST)
	public @ResponseBody Patient addPatient(@RequestBody Patient patientApi) {
		//Get current doctor from DB
		DoctorImpl doctor = getCurrentDoctor();		
		//Create patient from patientApi (if user doesn't exist)
		if (userService.userExists(patientApi.getUsername())) {
			throw new SecuredRestException(String.format("Patient with username %s already exists", patientApi.getUsername()));
		}
		PatientImpl patient = PatientImpl.createWithPatient(patientApi);
		for (Question question : patient.getQuestions()) {
			if (question.getServerId() == null) {
				mongoTemplate.save(question);
			}
		}
		//save patient
		userService.addUser(patient);
		//add patient to doctor
		doctor.addPatient(patient);
		//return patient (less authorities)
		mongoTemplate.save(doctor);
		return (Patient)credentialsLessAuthorities(patient);
	}
	
	@RequestMapping(value=ME_PATH, method=RequestMethod.GET)
	public @ResponseBody List<String> getUserRoles() {
		Credentials current = getCredentials();
		return current.getRoles();
	}

	@RequestMapping(value=DOCTOR_SVC_PATH + ME_PATH, method=RequestMethod.GET)
	public @ResponseBody Doctor getMyDetailsDoctor() {
		return (DoctorImpl)credentialsLessAuthorities(getCurrentDoctor());
	}

	@RequestMapping(value=PATIENT_SVC_PATH + ME_PATH, method=RequestMethod.GET)
	public @ResponseBody Patient getMyDetailsPatient() {
		return (PatientImpl)credentialsLessAuthorities(getCurrentPatient());
	}

	@RequestMapping(value=DOCTOR_SVC_PATH + ME_PATH, method=RequestMethod.PUT)
	public @ResponseBody Doctor updateMyDetails(@RequestBody Doctor doctor) {
		DoctorImpl me = getCurrentDoctor();
		if (!doctor.getUsername().equals(me.getUsername())) {
			throw new SecuredRestException("Authenticated username does not match");
		}
		safeEncodePassword(doctor, me.getPassword());
		me.updateDetails(doctor);
		mongoTemplate.save(me);
		return (DoctorImpl)credentialsLessAuthorities(me);
	}

	@RequestMapping(value=PATIENT_SVC_PATH + ME_PATH, method=RequestMethod.PUT)
	public @ResponseBody Patient updateMyDetails(@RequestBody Patient patient) {
		PatientImpl me = getCurrentPatient();
		if (!patient.getUsername().equals(me.getUsername())) {
			throw new SecuredRestException("Authenticated username does not match");
		}
		safeEncodePassword(patient, me.getPassword());
		for (Question question : patient.getQuestions()) {
			if (question.getServerId() == null) {
				mongoTemplate.save(question);
			}
		}
		me.updateDetails(patient);
		mongoTemplate.save(me);
		
		return (PatientImpl)credentialsLessAuthorities(me);
	}

	@RequestMapping(value=PATIENT_SVC_PATH, method=RequestMethod.GET)
	public @ResponseBody Collection<? extends Patient> getPatients() {
		DoctorImpl me = getCurrentDoctor();
		Collection<PatientImpl> patients = me.getPatients();
		safeListCredentials(patients);
		return patients;
	}

	@RequestMapping(value=PATIENT_SVC_PATH + BY_ID_PATH, method=RequestMethod.GET)
	public @ResponseBody Patient getPatient(@PathVariable String id) {
		return findPatientById(id);
	}
	
	private Patient findPatientById(String id) {
		DoctorImpl me = getCurrentDoctor();
		Collection<? extends Patient> patients = me.getPatients();
		Criteria criteria = Criteria.where("id").is(id);
		PatientImpl patient = (PatientImpl)mongoTemplate.findOne(Query.query(criteria), PatientImpl.class);
		if (patient == null || !patients.contains(patient)) {
			throw new SecuredRestException("Patient not found");
		}
		return patient;
	}

	@RequestMapping(value=PATIENT_SVC_PATH, method=RequestMethod.PUT)
	public @ResponseBody Patient updatePatient(@RequestBody Patient patient) {
		DoctorImpl me = getCurrentDoctor();
		Collection<? extends Patient> patients = me.getPatients();
		Criteria criteria = Criteria.where("username").is(patient.getUsername());
		PatientImpl myPatient = (PatientImpl)mongoTemplate.findOne(Query.query(criteria), PatientImpl.class);
		if (myPatient == null || !patients.contains(myPatient)) {
			throw new SecuredRestException("Patient not found");
		}
		for (Question question : patient.getQuestions()) {
			if (question.getServerId() == null) {
				mongoTemplate.save(question);
			}
		}
		safeEncodePassword(patient, myPatient.getPassword());
		myPatient.updateDetails(patient);
		mongoTemplate.save(myPatient);
		return myPatient;
	}

	@RequestMapping(value=PATIENT_SVC_PATH + SEARCH_NAME_PATH, method=RequestMethod.GET)
	public @ResponseBody Collection<? extends Patient> findPatientsByUsername(@RequestParam(UserSvcApi.USERNAME_PARAMETER) String username) {
		DoctorImpl me = getCurrentDoctor();
		Collection<PatientImpl> patients = me.getPatients();
		Collection<PatientImpl> matches = new ArrayList<PatientImpl>();
		for (PatientImpl patient : patients) {
			if (patient.getUsername().matches(String.format(".*%s.*", username))) {
				matches.add((PatientImpl)credentialsLessAuthorities(patient));
			}
		}
		return matches;
	}

	@RequestMapping(value=DOCTOR_SVC_PATH + MINE_PATH, method=RequestMethod.GET)
	public @ResponseBody Doctor getDoctor() {
		PatientImpl me = getCurrentPatient();
		Criteria criteria = Criteria.where("id").is(me.getDoctorRegistrationId());
		DoctorImpl doctor = (DoctorImpl)mongoTemplate.findOne(Query.query(criteria), DoctorImpl.class);
		if (doctor == null || !doctor.getRoles().contains(ROLE_DOCTOR)) {
			throw new SecuredRestException("Doctor not found");
		}
		return (DoctorImpl)credentialsLessAuthorities(doctor);
	}

	@RequestMapping(value=PATIENT_SVC_PATH + ME_PATH + CHECK_IN_PATH, method=RequestMethod.POST)
	public @ResponseBody CheckIn checkIn(@RequestBody CheckIn checkIn) {
		PatientImpl me = getCurrentPatient();
		CheckInImpl checkInImpl = (CheckInImpl) checkIn;
		checkInImpl.setSubmittedDate(new Date());
		me.addCheckIn(checkInImpl);
		mongoTemplate.save(me);
		return checkInImpl;
	}
	
	@RequestMapping(value=PATIENT_SVC_PATH + ME_PATH + CHECK_IN_PATH, method=RequestMethod.GET)
	public @ResponseBody Collection<? extends CheckIn> getMyCheckIns() {
		PatientImpl me = getCurrentPatient();

		return me.getCheckIns();
	}
	
	@RequestMapping(value=PATIENT_SVC_PATH + BY_ID_PATH + CHECK_IN_PATH, method=RequestMethod.GET)
	public @ResponseBody Collection<? extends CheckIn> getPatientCheckIns(@PathVariable String id) {
		Patient patient = findPatientById(id);
		return patient.getCheckIns();
	}
	
	



}
