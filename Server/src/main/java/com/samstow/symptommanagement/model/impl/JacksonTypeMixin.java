package com.samstow.symptommanagement.model.impl;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;

public class JacksonTypeMixin {

	@JsonDeserialize(as = DoctorImpl.class)
	public abstract class DoctorMixin{}

    @JsonDeserialize(as = PatientImpl.class)
    public abstract class PatientMixin{}

    @JsonDeserialize(as= QuestionImpl.class)
    public abstract class QuestionMixIn{}

    @JsonDeserialize(as= ResponseImpl.class)
    public abstract class ResponseMixIn{}

    @JsonDeserialize(as= CheckInImpl.class)
    public abstract class CheckInMixIn{}

}
