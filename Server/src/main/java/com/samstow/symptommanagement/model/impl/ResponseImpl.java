package com.samstow.symptommanagement.model.impl;


import com.samstow.symptommanagement.model.Response;
import com.samstow.symptommanagement.model.ResponseType;

import java.util.ArrayList;
import java.util.Date;

import org.springframework.data.annotation.PersistenceConstructor;

/**
 * Created by samstow on 30/11/14.
 */
public class ResponseImpl implements Response {
	
    protected String Id;
    private String question;
    private String responseText;
    private boolean response;
    private Date yesTime;
    private ResponseType responseType;
    private ArrayList<String> responses = new ArrayList<String>();

    @PersistenceConstructor
    public ResponseImpl() {

	}
    
    @Override
    public String getId() {
        return Id;
    }

    @Override
    public String getQuestion() {
        return question;
    }

    @Override
    public String getResponseText() {
        return responseText;
    }

    @Override
    public boolean getResponse() {
        return response;
    }

    @Override
    public Date getYesTime() {
        return yesTime;
    }

    @Override
    public ResponseType getResponseType() {
        return responseType;
    }

    @Override
    public ArrayList<String> getResponses() {
        return responses;
    }
}
