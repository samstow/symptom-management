package com.samstow.symptommanagement.model.impl;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;

import org.springframework.data.annotation.PersistenceConstructor;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.samstow.symptommanagement.client.UserSvcApi;
import com.samstow.symptommanagement.credentials.Credentials;
import com.samstow.symptommanagement.model.CheckIn;
import com.samstow.symptommanagement.model.Patient;
import com.samstow.symptommanagement.model.Question;
import com.samstow.symptommanagement.model.User;

@Document(collection="credentials")
public class PatientImpl extends Credentials implements Patient, User {

	/**
	 * 
	 */
	private static final long serialVersionUID = 3375505678472285201L;
	
	@Indexed
	private String patientRecordNumber;
	private Date dateOfBirth;
	private String doctorRegistrationId;
	
	@DBRef
	public Collection<Question> questions;
	
	@JsonIgnore
	private Collection<CheckIn> checkIns;
	
	@PersistenceConstructor
	public PatientImpl() {
		super();
		init();
	}
	
	private void init() {
		this.questions = new ArrayList<Question>();
		this.checkIns = new ArrayList<CheckIn>();
		addRole(UserSvcApi.ROLE_PATIENT);
	}
	
	public static PatientImpl createWithPatient(Patient patientApi) {
		return new PatientImpl(patientApi);
	}
	
    public static PatientImpl createWithUser(UserDetails user) {
    	return new PatientImpl(user);
    }
    
	public static PatientImpl create(String username, String password,
			String...authorities) {
		return new PatientImpl(username, password, authorities);
	}
	
	public static PatientImpl create(String username, String password,
			Collection<? extends GrantedAuthority> authorities) {
		return new PatientImpl(username, password, authorities);
	}
	
	public PatientImpl(String username, String password,
			Collection<? extends GrantedAuthority> authorities) {
		super(username, password, authorities);
		init();
	}

	public PatientImpl(String username, String password, String... authorities) {
		super(username, password, authorities);
		init();
	}

	public PatientImpl(UserDetails user) {
		super(user);
		init();
	}
	
	public PatientImpl(Patient patientApi) {
		super();
		init();
		setUsername(patientApi.getUsername());
		setPassword(patientApi.getPassword());
		setFirstName(patientApi.getFirstName());
		setLastName(patientApi.getLastName());
		setPhone(patientApi.getPhone());
		setPatientRecordNumber(patientApi.getPatientRecordNumber());
		if (patientApi.getCheckIns() != null) {
			this.checkIns = (Collection<CheckIn>) patientApi.getCheckIns();
		}
		if (patientApi.getQuestions() != null) {
			this.questions = (Collection<Question>) patientApi.getQuestions();
		}
	}
	
	public void updateDetails(Patient patientApi) {
		setUsername(patientApi.getUsername());
		setFirstName(patientApi.getFirstName());
		setLastName(patientApi.getLastName());
		setPassword(patientApi.getPassword());
		setPhone(patientApi.getPhone());
		setDateOfBirth(patientApi.getDateOfBirth());
		this.questions.clear(); 
		for (Question question :patientApi.getQuestions()) {
			this.questions.add(question);
		}
	}

	
	@Override
	public String getPatientRecordNumber() {
		return patientRecordNumber;
	}
	
	public void setPatientRecordNumber(String number) {
		this.patientRecordNumber = number;
	}

	@Override
	public Date getDateOfBirth() {
		return dateOfBirth;
	}

	@Override
	public Collection<? extends Question> getQuestions() {
		return questions;
	}

	@Override
	public String getDoctorRegistrationId() {
		return doctorRegistrationId;
	}
	
	
	public void setDoctorRegistrationId(String doctorId) {
		this.doctorRegistrationId = doctorId;
	}

	@Override
	public Collection<? extends CheckIn> getCheckIns() {
		return checkIns;
	}
	
	public void addCheckIn(CheckIn checkIn) {
		if (checkIn != null) {
			checkIns.add(checkIn); 
		}
	}

	@Override
	public void setDateOfBirth(Date dateOfBirth) {
		this.dateOfBirth = dateOfBirth;
		
	}

}
