package com.samstow.symptommanagement.model.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.Id;

import org.springframework.data.annotation.PersistenceConstructor;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;

import com.samstow.symptommanagement.model.Question;
import com.samstow.symptommanagement.model.QuestionType;
import com.samstow.symptommanagement.model.ResponseType;

/**
 * Created by samstow on 5/11/14.
 */
@Document
public class QuestionImpl implements Question, Serializable {

	@Id
    protected String id;
	protected String serverId;
	@Indexed
    private String questionText;
    private ArrayList<String> responseOptions = new ArrayList<String>();
    private int priority = 0;
    private ResponseType responseType;
    private QuestionType questionType;
    private String questionInfo;
    
    @PersistenceConstructor
    public QuestionImpl() {
	
	}
    
    public static QuestionImpl sampleQuestion() {
    	String medicationName = "test";
    	
    	String questionText = String.format("%s%s?", "Did you take the ", medicationName);
        QuestionImpl question = new QuestionImpl(
                questionText,
                null,
                ResponseType.DATED_BOOLEAN,
                QuestionType.MEDICATION,
                medicationName,
                0);
        return question;
    }
    
    @Override
    public String getServerId() {
        return id;
    }
    
    public void setServerId(String id) {
    	this.serverId = id;
    }
    
    public QuestionImpl(String text, ArrayList<String> responseOptions, ResponseType responseType, QuestionType questionType, String questionInfo, int priority) {
        this.questionText = text;
        this.responseOptions = responseOptions;
        this.responseType = responseType;
        this.priority = priority;
        this.questionType = questionType;
        this.questionInfo = questionInfo;
    }
    

    @Override
    public String getQuestionText() {
        return questionText;
    }

    @Override
    public List<String> getResponseOptions() {
        return responseOptions;
    }

    @Override
    public int getPriority() {
        return priority;
    }

    @Override
    public ResponseType getResponseType() {
        return responseType;
    }

    @Override
    public QuestionType getQuestionType() {
        return questionType;
    }

    @Override
    public String getQuestionInfo() {
        return questionInfo;
    }

    @Override
    public boolean equals(Object o) {
        if (o instanceof Question) {
            Question q = (Question)o;
            return q.getQuestionText().equals(questionText);
        }
        return super.equals(o);
    }
}
