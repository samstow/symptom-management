package com.samstow.symptommanagement.model.impl;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.springframework.data.annotation.PersistenceConstructor;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.samstow.symptommanagement.client.UserSvcApi;
import com.samstow.symptommanagement.credentials.Credentials;
import com.samstow.symptommanagement.model.Doctor;
import com.samstow.symptommanagement.model.Patient;

@Document(collection="credentials")
public class DoctorImpl extends Credentials implements Doctor {

	/**
	 * 
	 */
	private static final long serialVersionUID = -3023829247062845721L;
		
	@Indexed
	private String doctorRegistrationId;
	
	@JsonIgnore
	@DBRef
	private Collection<PatientImpl> patients;
	
	@PersistenceConstructor
	private DoctorImpl() {
		super();
		init();
	}
	
	private void init() {
		this.patients = new ArrayList<PatientImpl>();
		addRole(UserSvcApi.ROLE_DOCTOR);
	}
	
    public static DoctorImpl createWithUser(UserDetails user) {
    	return new DoctorImpl(user);
    }
    
	public static DoctorImpl create(String username, String password,
			String...authorities) {
		return new DoctorImpl(username, password, authorities);
	}
	
	public static DoctorImpl create(String username, String password,
			Collection<? extends GrantedAuthority> authorities) {
		return new DoctorImpl(username, password, authorities);
	}
	
	public DoctorImpl(String username, String password,
			Collection<? extends GrantedAuthority> authorities) {
		super(username, password, authorities);
		init();
	}

	public DoctorImpl(String username, String password, String... authorities) {
		super(username, password, authorities);
		init();
	}

	public DoctorImpl(UserDetails user) {
		super(user);
		init();
	}
	
	public void updateDetails(Doctor doctorApi) {
		setDoctorRegistrationId(doctorApi.getDoctorRegistrationId());
		setFirstName(doctorApi.getFirstName());
		setLastName(doctorApi.getLastName());
		setPassword(doctorApi.getPassword());
		setPhone(doctorApi.getPhone());
	}

	@Override
	public String getDoctorRegistrationId() {
		return getServerId();
	}
	
	public void setDoctorRegistrationId(String doctorRegistrationId) {
		this.doctorRegistrationId = doctorRegistrationId;
	}

	public void addPatient(PatientImpl patient) {
		if (!patients.contains(patient)) {
			this.patients.add(patient);
			patient.setDoctorRegistrationId(getDoctorRegistrationId());
		}
	}

//	@Override
//	public List<String> getPatientRecordNumbers() {
//		List<String> patientIds = new ArrayList<String>();
//		if (patients == null) return patientIds;
//		for (Patient patient : patients) {
//			patientIds.add(patient.getPatientRecordNumber());
//		}
//		return patientIds;
//	}
	
	@Override
	public Collection<PatientImpl> getPatients() {
		return patients;
	}


}
