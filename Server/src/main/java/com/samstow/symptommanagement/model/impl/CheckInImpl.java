package com.samstow.symptommanagement.model.impl;

import com.samstow.symptommanagement.model.CheckIn;
import com.samstow.symptommanagement.model.Response;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;

import javax.persistence.Id;

import org.springframework.data.annotation.PersistenceConstructor;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;

/**
 * Created by samstow on 30/11/14.
 */
public class CheckInImpl implements CheckIn {

    protected String id;

    private Collection<? extends Response> responses = new ArrayList<Response>();
    private Date time;
    private Date submittedDate;
    
    @PersistenceConstructor
    public CheckInImpl() {
	
	}

    @Override
    public String getId() {
        return id;
    }

    @Override
    public Collection<? extends Response> getResponses() {
        return responses;
    }

    @Override
    public Date getTime() {
        return time;
    }

    @Override
    public Date getSubmittedDate() {
        return submittedDate;
    }
    
    public void setSubmittedDate(Date date) {
    	this.submittedDate = date;
    }
}
