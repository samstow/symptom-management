package com.samstow.symptommanagement;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.data.mongodb.repository.config.EnableMongoRepositories;
import org.springframework.data.rest.webmvc.config.RepositoryRestMvcConfiguration;
import org.springframework.hateoas.ResourceSupport;
import org.springframework.hateoas.mvc.TypeConstrainedMappingJackson2HttpMessageConverter;
import org.springframework.http.MediaType;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.samstow.symptommanagement.auth.OAuth2SecurityConfiguration;
import com.samstow.symptommanagement.json.ResourcesMapper;
import com.samstow.symptommanagement.model.CheckIn;
import com.samstow.symptommanagement.model.Doctor;
import com.samstow.symptommanagement.model.Patient;
import com.samstow.symptommanagement.model.Question;
import com.samstow.symptommanagement.model.Response;
import com.samstow.symptommanagement.model.impl.JacksonTypeMixin;

//Tell Spring to automatically inject any dependencies that are marked in
//our classes with @Autowired
@EnableAutoConfiguration
//Tell Spring to automatically create a MongoDB implementation of our
//VideoRepository
@EnableMongoRepositories
//Tell Spring to turn on WebMVC (e.g., it should enable the DispatcherServlet
//so that requests can be routed to our Controllers)
@EnableWebMvc
// Tell Spring that this object represents a Configuration for the
// application
@Configuration
// Tell Spring to go and scan our controller package (and all sub packages) to
// find any Controllers or other components that are part of our applciation.
// Any class in this package that is annotated with @Controller is going to be
// automatically discovered and connected to the DispatcherServlet.
@ComponentScan
// We use the @Import annotation to include our OAuth2SecurityConfiguration
// as part of this configuration so that we can have security and oauth
// setup by Spring
@Import(OAuth2SecurityConfiguration.class)
public class Application extends RepositoryRestMvcConfiguration {
	
	@Bean
	public PasswordEncoder passwordEncoder() {
		return new BCryptPasswordEncoder();
	}

	// The app now requires that you pass the location of the keystore and
	// the password for your private key that you would like to setup HTTPS
	// with. In Eclipse, you can set these options by going to:
	//    1. Run->Run Configurations
	//    2. Under Java Applications, select your run configuration for this app
	//    3. Open the Arguments tab
	//    4. In VM Arguments, provide the following information to use the
	//       default keystore provided with the sample code:
	//
	//       -Dkeystore.file=src/main/resources/private/keystore -Dkeystore.pass=changeit
	//
	//    5. Note, this keystore is highly insecure! If you want more securtiy, you 
	//       should obtain a real SSL certificate:
	//
	//       http://tomcat.apache.org/tomcat-7.0-doc/ssl-howto.html
	//
	// Tell Spring to launch our app!
	public static void main(String[] args) {
		SpringApplication.run(Application.class, args);
	}
	
	// We are overriding the bean that RepositoryRestMvcConfiguration 
	// is using to convert our objects into JSON so that we can control
	// the format. The Spring dependency injection will inject our instance
	// of ObjectMapper in all of the spring data rest classes that rely
	// on the ObjectMapper. This is an example of how Spring dependency
	// injection allows us to easily configure dependencies in code that
	// we don't have easy control over otherwise.
	@Override
	public ObjectMapper halObjectMapper(){
		ObjectMapper mapper = new ResourcesMapper();
		configureJacksonObjectMapper(mapper);
		return mapper;
	}
	
	@Override
	protected void configureJacksonObjectMapper(ObjectMapper objectMapper) {
		objectMapper.addMixInAnnotations(Patient.class, JacksonTypeMixin.PatientMixin.class);
        objectMapper.addMixInAnnotations(Doctor.class, JacksonTypeMixin.DoctorMixin.class);
        objectMapper.addMixInAnnotations(Question.class, JacksonTypeMixin.QuestionMixIn.class);
        objectMapper.addMixInAnnotations(Response.class, JacksonTypeMixin.ResponseMixIn.class);
        objectMapper.addMixInAnnotations(CheckIn.class, JacksonTypeMixin.CheckInMixIn.class);
	}
	
	public MappingJackson2HttpMessageConverter extraJacksonHttpMessageConverter() {
		MappingJackson2HttpMessageConverter messageConverter = new MappingJackson2HttpMessageConverter();
		List<MediaType> mediaTypes = new ArrayList<MediaType>();
		mediaTypes.addAll(Arrays.asList(MediaType.valueOf("application/schema+json"),
				MediaType.valueOf("application/x-spring-data-verbose+json"),
				MediaType.valueOf("application/x-spring-data-compact+json")));
			mediaTypes.add(MediaType.APPLICATION_JSON);

		messageConverter.setObjectMapper(objectMapper());
		messageConverter.setSupportedMediaTypes(mediaTypes);
		
		return messageConverter;
	}
	
	@Override
	public void configureMessageConverters(
			List<HttpMessageConverter<?>> converters) {
		converters.add(extraJacksonHttpMessageConverter());
	}
	
	@Override
	protected void configureHttpMessageConverters(
			List<HttpMessageConverter<?>> messageConverters) {
		messageConverters.add(extraJacksonHttpMessageConverter());
	}
	

}
