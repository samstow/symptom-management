package com.samstow.symptommanagement.persistence;

import java.math.BigInteger;
import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.convert.MappingMongoConverter;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;

import com.mongodb.BasicDBObjectBuilder;
import com.mongodb.DBObject;
import com.samstow.symptommanagement.model.impl.PatientImpl;

@Component
public class PatientWriteConverter implements Converter<PatientImpl, DBObject> {

	@Autowired
	CredentialsWriteConverter credentialsConverter;
	
	@Override
	public DBObject convert(PatientImpl source) {
	    DBObject dbo = credentialsConverter.convert(source);
		dbo.put("dateOfBirth", source.getDateOfBirth());
	    dbo.put("patientRecordNumber", source.getPatientRecordNumber());
	    dbo.put("checkIns", credentialsConverter.mongoConverter.convertToMongoType(source.getCheckIns()));
	    return dbo;
	}

}
