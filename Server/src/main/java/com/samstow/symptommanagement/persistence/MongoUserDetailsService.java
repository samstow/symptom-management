package com.samstow.symptommanagement.persistence;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;

import com.samstow.symptommanagement.credentials.Credentials;

/**
 * @author Moritz Schulze
 */
public class MongoUserDetailsService implements UserDetailsService {

	@Autowired
	private MongoTemplate template;
	
	@Autowired
	private PasswordEncoder encoder;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
    	Criteria criteria = Criteria.where("username").is(username);
        Credentials credentials = template.findOne(Query.query(criteria), Credentials.class);
        if(credentials == null) {
            throw new UsernameNotFoundException("User " + username + " not found in database.");
        }
        
        return credentials;
    }
    
    public boolean userExists(String userName) {
    	UserDetails userDetails = null;
    	try {
    		userDetails =loadUserByUsername(userName);
    	} catch (UsernameNotFoundException ex) {
    		return false;
    	}
    	return userDetails != null;
    }
    
    public UserDetails addUser(Credentials user) {
    	user.setPassword(encoder.encode(user.getPassword()));
    	return saveUser(user);
    }
    public UserDetails saveUser(Credentials user) {
    	template.save(user);
    	UserDetails userDetails = loadUserByUsername(user.getUsername()); 
    	return userDetails;
    }
}
