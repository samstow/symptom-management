package com.samstow.symptommanagement.persistence;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.convert.converter.Converter;
import org.springframework.data.mongodb.MongoDbFactory;
import org.springframework.data.mongodb.config.AbstractMongoConfiguration;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.SimpleMongoDbFactory;
import org.springframework.data.mongodb.core.convert.CustomConversions;
import org.springframework.data.mongodb.core.convert.DefaultDbRefResolver;
import org.springframework.data.mongodb.core.convert.MappingMongoConverter;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;

import com.mongodb.BasicDBObjectBuilder;
import com.mongodb.DBObject;
import com.mongodb.Mongo;
import com.mongodb.MongoClient;

@Configuration
public class MongoConfiguration extends AbstractMongoConfiguration {
	
	@Value("${spring.data.mongodb.database}")
    private String databaseName;

  public @Bean MongoDbFactory mongoDbFactory() throws Exception {
    return new SimpleMongoDbFactory(mongo(), databaseName);
  }

  public @Bean MongoTemplate mongoTemplate() throws Exception {
      final MongoTemplate template = new MongoTemplate(
              mongoDbFactory(), mappingMongoConverter());
      return template;
  }
  
  @Autowired
  CredentialsWriteConverter credentialsWriteConverter;
  
  @Autowired
  DoctorWriteConverter doctorWriteConverter;
  
  @Autowired
  PatientWriteConverter patientWriteConverter;
  
  @Override
  @Bean
  public CustomConversions customConversions() {
      List<Converter <?, ?>> converters = new ArrayList<Converter<?, ?>>();
      converters.add(credentialsWriteConverter);
      converters.add(doctorWriteConverter);
      converters.add(patientWriteConverter);
      return new CustomConversions(converters);
  }

  @Override
  @Bean
  public MappingMongoConverter mappingMongoConverter() throws Exception {
      MappingMongoConverter converter = new MappingMongoConverter(
              new DefaultDbRefResolver(mongoDbFactory()), mongoMappingContext());
//      converter.setCustomConversions(customConversions());
      return converter;
  }

@Override
protected String getDatabaseName() {
	return databaseName;
}

@Override
public Mongo mongo() throws Exception {
	return new MongoClient();
}

}