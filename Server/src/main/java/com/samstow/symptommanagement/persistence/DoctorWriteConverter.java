package com.samstow.symptommanagement.persistence;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.convert.MappingMongoConverter;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;

import com.mongodb.BasicDBObjectBuilder;
import com.mongodb.DBObject;
import com.samstow.symptommanagement.model.impl.DoctorImpl;

@Component
public class DoctorWriteConverter implements Converter<DoctorImpl, DBObject> {

	@Autowired
	CredentialsWriteConverter credentialsConverter;
	
	@Override
	public DBObject convert(DoctorImpl source) {
	    DBObject dbo = credentialsConverter.convert(source);
	    dbo.put("doctorRegistrationId", source.getDoctorRegistrationId());
	    return dbo;
	}

}
