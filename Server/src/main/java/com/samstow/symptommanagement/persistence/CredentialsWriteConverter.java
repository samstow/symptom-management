package com.samstow.symptommanagement.persistence;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springframework.data.mongodb.core.convert.MappingMongoConverter;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;

import com.mongodb.BasicDBObjectBuilder;
import com.mongodb.DBObject;
import com.samstow.symptommanagement.credentials.Credentials;

@Component
public class CredentialsWriteConverter implements Converter<Credentials, DBObject> {

	@Autowired
	PasswordEncoder passwordEncoder;
	
	@Autowired
	public MappingMongoConverter mongoConverter;
	
	@Override
	public DBObject convert(Credentials source) {
		String encoded  = passwordEncoder.encode(source.getPassword());
		source.setPassword(encoded);
	    DBObject dbo = new BasicDBObjectBuilder()
	    .append("_id", source.getServerId())
	    .append("firstName", source.getFirstName())
	    .append("lastName", source.getLastName())
	    .append("password", encoded)
	    .append("username", source.getUsername())
	    .append("phone", source.getPhone())
	    .append("authorities", mongoConverter.convertToMongoType(source.getAuthorities()))
	    .append("enabled", source.isEnabled())
	    .get();
	    return dbo;
	}

}
