package com.samstow.symptommanagement.credentials;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Collection;

import javax.persistence.Id;

import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.security.core.GrantedAuthority;

/**
 * @author Moritz Schulze
 */
@Document
public class Authority implements GrantedAuthority {

    /**
	 * 
	 */
	private static final long serialVersionUID = -7541045615094940300L;

	@Id
    private BigInteger id;

    private String authority;
    
    public Authority(String authority)
    {
    	this.authority = authority;
    }

    public BigInteger getId() {
        return id;
    }

    public void setId(BigInteger id) {
        this.id = id;
    }

    @Override
    public String getAuthority() {
        return authority;
    }

    public void setAuthority(String authority) {
        this.authority = authority;
    }
    
    static public Collection<Authority> createAuthorities(Collection<? extends GrantedAuthority> authorities) {
    	Collection<Authority> returnAuthorities = new ArrayList<Authority>();
    	for (GrantedAuthority authority : authorities) {
    		returnAuthorities.add(new Authority(authority.getAuthority()));
    	}
    	return returnAuthorities;
    }
    
    static public Collection<Authority> createAuthorities(String... authorities) {
    	Collection<Authority> returnAuthorities = new ArrayList<Authority>();
    	for (String authority : authorities) {
    		returnAuthorities.add(new Authority(authority));
    	}
    	return returnAuthorities;
    }
}
