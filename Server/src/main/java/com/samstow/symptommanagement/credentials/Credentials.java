package com.samstow.symptommanagement.credentials;


import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

import javax.persistence.Id;

import org.springframework.data.annotation.PersistenceConstructor;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.google.common.base.Objects;
import com.samstow.symptommanagement.model.User;

/**
 * @author Moritz Schulze
 */
@Document
public class Credentials implements UserDetails, User {

    /**
	 * 
	 */
	private static final long serialVersionUID = -819365982684611956L;
	
	@Id
    private String id;
	
	private String serverId;

	@JsonIgnore
    private Integer version;

    @Indexed
    private String username;

    private String password;
    
    private String firstName;
    
    private String lastName;
    
    private String phone;
    
    @JsonIgnore
    protected Collection<? extends GrantedAuthority> authorities;

    @JsonIgnore
    private boolean enabled;

    public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

    public String getServerId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
    
    public void setServerId(String id) {
    	this.serverId = id;
    }
    
    public static Credentials createWithUser(UserDetails user) {
    	return new Credentials(user);
    }
    
	public static Credentials create(String username, String password,
			String...authorities) {
		return new Credentials(username, password, authorities);
	}
	
	public static Credentials create(String username, String password,
			Collection<? extends GrantedAuthority> authorities) {
		return new Credentials(username, password, authorities);
	}


	@SuppressWarnings("unchecked")
	private Credentials(String username, String password) {
		this(username, password, Collections.EMPTY_LIST);
	}

	protected Credentials(String username, String password,
			String... authorities) {
		this.username = username;
		this.password = password;
		this.authorities = Authority.createAuthorities(authorities);
    	this.firstName = "";
    	this.lastName = "";
    	this.phone = "";
    	this.enabled = true;
	}

	protected Credentials(String username, String password,
			Collection<? extends GrantedAuthority> authorities) {
		super();
		this.username = username;
		this.password = password;
		this.authorities = authorities;
    	this.firstName = "";
    	this.lastName = "";
    	this.phone = "";
    	this.enabled = true;
	}
    
	@PersistenceConstructor
    protected Credentials() {
    	this.username = "";
    	this.password = "";
    	this.authorities = new ArrayList<Authority>();
    	this.enabled = true;
    	this.firstName = "";
    	this.lastName = "";
    	this.phone = "";
    }
    
    protected Credentials(UserDetails user) {
    	this.username = user.getUsername();
    	this.password = user.getPassword();
    	this.authorities = user.getAuthorities();
    	this.enabled = user.isEnabled();
    	this.firstName = "";
    	this.lastName = "";
    	this.phone = "";
    }

    public Integer getVersion() {
        return version;
    }

    public void setVersion(Integer version) {
        this.version = version;
    }

    public String getPassword() {
        return password;
    }
    
	@Override
	public String getUsername() {
		return username;
	}
	
    public void setUsername(String name) {
        this.username = name;
    }
    
    public void addRole(String role) {
    	ArrayList<GrantedAuthority> newAuthorities = new ArrayList<>(authorities);
		newAuthorities.add(new Authority(role));
		this.authorities = newAuthorities;
    }
    
    public List<String> getRoles() {
    	List<String> roles = new ArrayList<String>();
    	for (GrantedAuthority authority : authorities) {
    		roles.add(authority.getAuthority());
    	}
    	return roles;
    }

	@Override
	public boolean isAccountNonExpired() {
		return true;
	}

	@Override
	public boolean isAccountNonLocked() {
		return true;
	}

	@Override
	public boolean isCredentialsNonExpired() {
		return true;
	}

    public void setPassword(String password) {
        this.password = password;
    }

    public Collection<? extends GrantedAuthority> getAuthorities() {
        return authorities;
    }

    public void setAuthorities(List<Authority> authorities) {
        this.authorities = authorities;
    }

    public boolean isEnabled() {
        return enabled;
    }

    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }
    
	/**
	 * Two Credentials will generate the same hashcode if they have exactly the same
	 * values for their name and password.
	 * 
	 */
	@Override
	public int hashCode() {
		// Google Guava provides great utilities for hashing
		return Objects.hashCode(username, password);
	}

	/**
	 * Two Videos are considered equal if they have exactly the same values for
	 * their name and password.
	 * 
	 */
	@Override
	public boolean equals(Object obj) {
		if (obj instanceof Credentials) {
			Credentials other = (Credentials) obj;
			// Google Guava provides great utilities for equals too!
			return Objects.equal(username, other.username)
					&& Objects.equal(password, other.password);
		} else {
			return false;
		}
	}
}
