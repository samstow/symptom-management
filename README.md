# README #

### What is this repository for? ###

* Symptom Management Application for Coursera Capstone project

### How do I get set up? ###

* Requires a mongoDB installation running on local machine
* Requires genymotion device (or change the localhost address in the android client)
* Both server and app modules require the 'Common' module to be included in project / workspace.
* Uses grade so all other dependencies should be autoconfigured
* There are some brief jUnit tests in the Server project

### Set up for android studio ###

* Git clone the root repository 
* Open Android Studio and select 'Import Project' 
* IMPORTANT - do not select 'Open Project' as it won't set up the 'Common' dependency
* Import the project in the 'SymptomManagement directory, not the root directory
* Because of the Android Annotations dependency, need to build the app once for the activities in the manifest to be found 
* In order to use the app, you will need the Server running, this is a separate set-up process, requires eclipse with Java-Spring module installed to build and run, also a running instance of Mongo DB.

Please browse the source code.